import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisProperties;

import at.chipkarte.client.base.soap.BaseService;
import at.chipkarte.client.base.soap.BaseServiceLocator;
import at.chipkarte.client.base.soap.CardReader;
import at.chipkarte.client.base.soap.IBaseService;

public class Test {

	public static void main(String[] args) throws Exception 
	{
        AxisProperties.setProperty("axis.socketSecureFactory",
        		"org.apache.axis.components.net.SunFakeTrustSocketFactory");

		
		BaseServiceLocator locator = new BaseServiceLocator();
		IBaseService client = locator.getbase_15();
		
		CardReader[] readers = client.getCardReaders();
		System.out.println(readers[0].getId());
		

	}

}
