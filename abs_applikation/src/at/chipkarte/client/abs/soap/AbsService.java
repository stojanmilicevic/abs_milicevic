/**
 * AbsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public interface AbsService extends javax.xml.rpc.Service {
    public java.lang.String getabs_11Address();

    public at.chipkarte.client.abs.soap.IAbsService getabs_11() throws javax.xml.rpc.ServiceException;

    public at.chipkarte.client.abs.soap.IAbsService getabs_11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
