/**
 * AbsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class AbsServiceLocator extends org.apache.axis.client.Service implements at.chipkarte.client.abs.soap.AbsService {

    public AbsServiceLocator() {
    }


    public AbsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AbsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for abs_11
    private java.lang.String abs_11_address = "https://10.196.2.18/abs/11";

    public java.lang.String getabs_11Address() {
        return abs_11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String abs_11WSDDServiceName = "abs_11";

    public java.lang.String getabs_11WSDDServiceName() {
        return abs_11WSDDServiceName;
    }

    public void setabs_11WSDDServiceName(java.lang.String name) {
        abs_11WSDDServiceName = name;
    }

    public at.chipkarte.client.abs.soap.IAbsService getabs_11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(abs_11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getabs_11(endpoint);
    }

    public at.chipkarte.client.abs.soap.IAbsService getabs_11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            at.chipkarte.client.abs.soap.Abs_11BindingStub _stub = new at.chipkarte.client.abs.soap.Abs_11BindingStub(portAddress, this);
            _stub.setPortName(getabs_11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setabs_11EndpointAddress(java.lang.String address) {
        abs_11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (at.chipkarte.client.abs.soap.IAbsService.class.isAssignableFrom(serviceEndpointInterface)) {
                at.chipkarte.client.abs.soap.Abs_11BindingStub _stub = new at.chipkarte.client.abs.soap.Abs_11BindingStub(new java.net.URL(abs_11_address), this);
                _stub.setPortName(getabs_11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("abs_11".equals(inputPortName)) {
            return getabs_11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "AbsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "abs_11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("abs_11".equals(portName)) {
            setabs_11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
