/**
 * Abs_11BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class Abs_11BindingStub extends org.apache.axis.client.Stub implements at.chipkarte.client.abs.soap.IAbsService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[9];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("abfragenLangzeitbewilligung");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "sVNRPatient"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitbewilligungsAbfrageErgebnis"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterBewilligungsanfrageException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "AbsException"),
                      "at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "at.chipkarte.client.base.soap.exceptions.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property"));
        oper.setReturnClass(at.chipkarte.client.base.soap.Property[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ermittelnPatientenDaten");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "svNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "svtCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "ekvkNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "geschlecht"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vorname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "antragstyp"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.BewilligungsAnfrage.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterBewilligungsanfrageException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "AbsException"),
                      "at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "at.chipkarte.client.base.soap.exceptions.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRueckantwort");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "rueckantwort"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.Rueckantwort.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "AbsException"),
                      "at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterRueckantwortException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterRueckantwortExceptionContent"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSVTs");
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "svtProperty"));
        oper.setReturnClass(at.chipkarte.client.base.soap.SvtProperty[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getStatusBewilligungsAnfragen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "filterkriterien"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "filterKriterien"), at.chipkarte.client.abs.soap.FilterKriterien.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "statusBewilligungsAnfrage"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterGetAnfrageStatusException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterGetAnfrageStatusExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getStatusPatientBewilligungsAnfragen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "svNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vorname"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "statusBewilligungsAnfrage"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterBewilligungsanfrageException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterGetAnfrageStatusException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterGetAnfrageStatusExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "at.chipkarte.client.base.soap.exceptions.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendenAnfrage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage"), at.chipkarte.client.abs.soap.BewilligungsAnfrage.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "attachment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"), byte[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageAntwort"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.AnfrageAntwort.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterBewilligungsanfrageException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "AbsException"),
                      "at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "at.chipkarte.client.base.soap.exceptions.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendenFolgeverordnung");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "ausstellungsParameter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsParameter"), at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsParameter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsErgebnis"));
        oper.setReturnClass(at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "InvalidParameterBewilligungsanfrageException"),
                      "at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "at.chipkarte.client.base.soap.exceptions.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "at.chipkarte.client.base.soap.exceptions.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "AbsException"),
                      "at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "at.chipkarte.client.base.soap.exceptions.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[8] = oper;

    }

    public Abs_11BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Abs_11BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Abs_11BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterBewilligungsanfrageExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterGetAnfrageStatusExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "invalidParameterRueckantwortExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.exceptions.AccessExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "baseExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.exceptions.BaseExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.exceptions.CardExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.exceptions.DialogExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageAntwort");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.AnfrageAntwort.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageEntscheid");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.AnfrageEntscheid.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.BewilligungsAnfrage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "filterKriterien");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.FilterKriterien.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsErgebnis");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsParameter");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsParameter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitbewilligungsAbfrageErgebnis");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitVerordnung");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.LangzeitVerordnung.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikament");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.Medikament.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.PatientenDaten.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "rueckantwort");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.Rueckantwort.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "statusBewilligungsAnfrage");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnung");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.abs.soap.Verordnung.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "baseProperty");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.BaseProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.Property.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "svtProperty");
            cachedSerQNames.add(qName);
            cls = at.chipkarte.client.base.soap.SvtProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[] abfragenLangzeitbewilligung(java.lang.String dialogId, java.lang.String sVNRPatient, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "abfragenLangzeitbewilligung"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, sVNRPatient, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[]) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.CardExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.base.soap.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "checkStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.base.soap.Property[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.base.soap.Property[]) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.base.soap.Property[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.BewilligungsAnfrage ermittelnPatientenDaten(java.lang.String dialogId, java.lang.String svNummer, java.lang.String svtCode, java.lang.String ekvkNummer, java.lang.String geschlecht, java.lang.String nachname, java.lang.String vorname, java.lang.String antragstyp, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "ermittelnPatientenDaten"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, svNummer, svtCode, ekvkNummer, geschlecht, nachname, vorname, antragstyp, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.BewilligungsAnfrage) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.BewilligungsAnfrage) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.BewilligungsAnfrage.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.CardExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.Rueckantwort getRueckantwort(java.lang.String dialogId, java.lang.String anfrageId) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "getRueckantwort"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, anfrageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.Rueckantwort) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.Rueckantwort) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.Rueckantwort.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.base.soap.SvtProperty[] getSVTs() throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "getSVTs"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.base.soap.SvtProperty[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.base.soap.SvtProperty[]) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.base.soap.SvtProperty[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[] getStatusBewilligungsAnfragen(java.lang.String dialogId, at.chipkarte.client.abs.soap.FilterKriterien filterkriterien) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "getStatusBewilligungsAnfragen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, filterkriterien});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[]) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[] getStatusPatientBewilligungsAnfragen(java.lang.String dialogId, java.lang.String svNummer, java.lang.String nachname, java.lang.String vorname, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "getStatusPatientBewilligungsAnfragen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, svNummer, nachname, vorname, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[]) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.CardExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.AnfrageAntwort sendenAnfrage(java.lang.String dialogId, at.chipkarte.client.abs.soap.BewilligungsAnfrage anfrage, java.lang.String cardReaderId, byte[] attachment) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "sendenAnfrage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, anfrage, cardReaderId, attachment});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.AnfrageAntwort) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.AnfrageAntwort) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.AnfrageAntwort.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.CardExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis sendenFolgeverordnung(java.lang.String dialogId, at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsParameter ausstellungsParameter, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "sendenFolgeverordnung"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, ausstellungsParameter, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis) _resp;
            } catch (java.lang.Exception _exception) {
                return (at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis) org.apache.axis.utils.JavaUtils.convert(_resp, at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) {
              throw (at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof at.chipkarte.client.base.soap.exceptions.CardExceptionContent) {
              throw (at.chipkarte.client.base.soap.exceptions.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
