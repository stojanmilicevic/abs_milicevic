/**
 * AnfrageAntwort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class AnfrageAntwort  implements java.io.Serializable {
    private java.lang.Boolean bearbeitungNaechsterWerktag;

    private java.lang.String id;

    private at.chipkarte.client.abs.soap.PatientenDaten patientenDaten;

    public AnfrageAntwort() {
    }

    public AnfrageAntwort(
           java.lang.Boolean bearbeitungNaechsterWerktag,
           java.lang.String id,
           at.chipkarte.client.abs.soap.PatientenDaten patientenDaten) {
           this.bearbeitungNaechsterWerktag = bearbeitungNaechsterWerktag;
           this.id = id;
           this.patientenDaten = patientenDaten;
    }


    /**
     * Gets the bearbeitungNaechsterWerktag value for this AnfrageAntwort.
     * 
     * @return bearbeitungNaechsterWerktag
     */
    public java.lang.Boolean getBearbeitungNaechsterWerktag() {
        return bearbeitungNaechsterWerktag;
    }


    /**
     * Sets the bearbeitungNaechsterWerktag value for this AnfrageAntwort.
     * 
     * @param bearbeitungNaechsterWerktag
     */
    public void setBearbeitungNaechsterWerktag(java.lang.Boolean bearbeitungNaechsterWerktag) {
        this.bearbeitungNaechsterWerktag = bearbeitungNaechsterWerktag;
    }


    /**
     * Gets the id value for this AnfrageAntwort.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this AnfrageAntwort.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the patientenDaten value for this AnfrageAntwort.
     * 
     * @return patientenDaten
     */
    public at.chipkarte.client.abs.soap.PatientenDaten getPatientenDaten() {
        return patientenDaten;
    }


    /**
     * Sets the patientenDaten value for this AnfrageAntwort.
     * 
     * @param patientenDaten
     */
    public void setPatientenDaten(at.chipkarte.client.abs.soap.PatientenDaten patientenDaten) {
        this.patientenDaten = patientenDaten;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AnfrageAntwort)) return false;
        AnfrageAntwort other = (AnfrageAntwort) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bearbeitungNaechsterWerktag==null && other.getBearbeitungNaechsterWerktag()==null) || 
             (this.bearbeitungNaechsterWerktag!=null &&
              this.bearbeitungNaechsterWerktag.equals(other.getBearbeitungNaechsterWerktag()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.patientenDaten==null && other.getPatientenDaten()==null) || 
             (this.patientenDaten!=null &&
              this.patientenDaten.equals(other.getPatientenDaten())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBearbeitungNaechsterWerktag() != null) {
            _hashCode += getBearbeitungNaechsterWerktag().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getPatientenDaten() != null) {
            _hashCode += getPatientenDaten().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AnfrageAntwort.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageAntwort"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bearbeitungNaechsterWerktag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bearbeitungNaechsterWerktag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientenDaten");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
