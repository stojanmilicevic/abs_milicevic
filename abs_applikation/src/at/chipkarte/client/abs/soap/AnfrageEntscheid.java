/**
 * AnfrageEntscheid.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class AnfrageEntscheid  implements java.io.Serializable {
    private java.lang.Integer beantragtePackungsanzahl;

    private at.chipkarte.client.abs.soap.Medikament beantragtesMedikament;

    private java.lang.Integer bewilligtePackungsanzahl;

    private at.chipkarte.client.abs.soap.Medikament bewilligtesMedikament;

    private java.lang.String infoText;

    private at.chipkarte.client.abs.soap.LangzeitVerordnung langzeitVerordnung;

    private java.lang.String magistraleZubereitung;

    private java.lang.String verordnungsEntscheidung;

    public AnfrageEntscheid() {
    }

    public AnfrageEntscheid(
           java.lang.Integer beantragtePackungsanzahl,
           at.chipkarte.client.abs.soap.Medikament beantragtesMedikament,
           java.lang.Integer bewilligtePackungsanzahl,
           at.chipkarte.client.abs.soap.Medikament bewilligtesMedikament,
           java.lang.String infoText,
           at.chipkarte.client.abs.soap.LangzeitVerordnung langzeitVerordnung,
           java.lang.String magistraleZubereitung,
           java.lang.String verordnungsEntscheidung) {
           this.beantragtePackungsanzahl = beantragtePackungsanzahl;
           this.beantragtesMedikament = beantragtesMedikament;
           this.bewilligtePackungsanzahl = bewilligtePackungsanzahl;
           this.bewilligtesMedikament = bewilligtesMedikament;
           this.infoText = infoText;
           this.langzeitVerordnung = langzeitVerordnung;
           this.magistraleZubereitung = magistraleZubereitung;
           this.verordnungsEntscheidung = verordnungsEntscheidung;
    }


    /**
     * Gets the beantragtePackungsanzahl value for this AnfrageEntscheid.
     * 
     * @return beantragtePackungsanzahl
     */
    public java.lang.Integer getBeantragtePackungsanzahl() {
        return beantragtePackungsanzahl;
    }


    /**
     * Sets the beantragtePackungsanzahl value for this AnfrageEntscheid.
     * 
     * @param beantragtePackungsanzahl
     */
    public void setBeantragtePackungsanzahl(java.lang.Integer beantragtePackungsanzahl) {
        this.beantragtePackungsanzahl = beantragtePackungsanzahl;
    }


    /**
     * Gets the beantragtesMedikament value for this AnfrageEntscheid.
     * 
     * @return beantragtesMedikament
     */
    public at.chipkarte.client.abs.soap.Medikament getBeantragtesMedikament() {
        return beantragtesMedikament;
    }


    /**
     * Sets the beantragtesMedikament value for this AnfrageEntscheid.
     * 
     * @param beantragtesMedikament
     */
    public void setBeantragtesMedikament(at.chipkarte.client.abs.soap.Medikament beantragtesMedikament) {
        this.beantragtesMedikament = beantragtesMedikament;
    }


    /**
     * Gets the bewilligtePackungsanzahl value for this AnfrageEntscheid.
     * 
     * @return bewilligtePackungsanzahl
     */
    public java.lang.Integer getBewilligtePackungsanzahl() {
        return bewilligtePackungsanzahl;
    }


    /**
     * Sets the bewilligtePackungsanzahl value for this AnfrageEntscheid.
     * 
     * @param bewilligtePackungsanzahl
     */
    public void setBewilligtePackungsanzahl(java.lang.Integer bewilligtePackungsanzahl) {
        this.bewilligtePackungsanzahl = bewilligtePackungsanzahl;
    }


    /**
     * Gets the bewilligtesMedikament value for this AnfrageEntscheid.
     * 
     * @return bewilligtesMedikament
     */
    public at.chipkarte.client.abs.soap.Medikament getBewilligtesMedikament() {
        return bewilligtesMedikament;
    }


    /**
     * Sets the bewilligtesMedikament value for this AnfrageEntscheid.
     * 
     * @param bewilligtesMedikament
     */
    public void setBewilligtesMedikament(at.chipkarte.client.abs.soap.Medikament bewilligtesMedikament) {
        this.bewilligtesMedikament = bewilligtesMedikament;
    }


    /**
     * Gets the infoText value for this AnfrageEntscheid.
     * 
     * @return infoText
     */
    public java.lang.String getInfoText() {
        return infoText;
    }


    /**
     * Sets the infoText value for this AnfrageEntscheid.
     * 
     * @param infoText
     */
    public void setInfoText(java.lang.String infoText) {
        this.infoText = infoText;
    }


    /**
     * Gets the langzeitVerordnung value for this AnfrageEntscheid.
     * 
     * @return langzeitVerordnung
     */
    public at.chipkarte.client.abs.soap.LangzeitVerordnung getLangzeitVerordnung() {
        return langzeitVerordnung;
    }


    /**
     * Sets the langzeitVerordnung value for this AnfrageEntscheid.
     * 
     * @param langzeitVerordnung
     */
    public void setLangzeitVerordnung(at.chipkarte.client.abs.soap.LangzeitVerordnung langzeitVerordnung) {
        this.langzeitVerordnung = langzeitVerordnung;
    }


    /**
     * Gets the magistraleZubereitung value for this AnfrageEntscheid.
     * 
     * @return magistraleZubereitung
     */
    public java.lang.String getMagistraleZubereitung() {
        return magistraleZubereitung;
    }


    /**
     * Sets the magistraleZubereitung value for this AnfrageEntscheid.
     * 
     * @param magistraleZubereitung
     */
    public void setMagistraleZubereitung(java.lang.String magistraleZubereitung) {
        this.magistraleZubereitung = magistraleZubereitung;
    }


    /**
     * Gets the verordnungsEntscheidung value for this AnfrageEntscheid.
     * 
     * @return verordnungsEntscheidung
     */
    public java.lang.String getVerordnungsEntscheidung() {
        return verordnungsEntscheidung;
    }


    /**
     * Sets the verordnungsEntscheidung value for this AnfrageEntscheid.
     * 
     * @param verordnungsEntscheidung
     */
    public void setVerordnungsEntscheidung(java.lang.String verordnungsEntscheidung) {
        this.verordnungsEntscheidung = verordnungsEntscheidung;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AnfrageEntscheid)) return false;
        AnfrageEntscheid other = (AnfrageEntscheid) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beantragtePackungsanzahl==null && other.getBeantragtePackungsanzahl()==null) || 
             (this.beantragtePackungsanzahl!=null &&
              this.beantragtePackungsanzahl.equals(other.getBeantragtePackungsanzahl()))) &&
            ((this.beantragtesMedikament==null && other.getBeantragtesMedikament()==null) || 
             (this.beantragtesMedikament!=null &&
              this.beantragtesMedikament.equals(other.getBeantragtesMedikament()))) &&
            ((this.bewilligtePackungsanzahl==null && other.getBewilligtePackungsanzahl()==null) || 
             (this.bewilligtePackungsanzahl!=null &&
              this.bewilligtePackungsanzahl.equals(other.getBewilligtePackungsanzahl()))) &&
            ((this.bewilligtesMedikament==null && other.getBewilligtesMedikament()==null) || 
             (this.bewilligtesMedikament!=null &&
              this.bewilligtesMedikament.equals(other.getBewilligtesMedikament()))) &&
            ((this.infoText==null && other.getInfoText()==null) || 
             (this.infoText!=null &&
              this.infoText.equals(other.getInfoText()))) &&
            ((this.langzeitVerordnung==null && other.getLangzeitVerordnung()==null) || 
             (this.langzeitVerordnung!=null &&
              this.langzeitVerordnung.equals(other.getLangzeitVerordnung()))) &&
            ((this.magistraleZubereitung==null && other.getMagistraleZubereitung()==null) || 
             (this.magistraleZubereitung!=null &&
              this.magistraleZubereitung.equals(other.getMagistraleZubereitung()))) &&
            ((this.verordnungsEntscheidung==null && other.getVerordnungsEntscheidung()==null) || 
             (this.verordnungsEntscheidung!=null &&
              this.verordnungsEntscheidung.equals(other.getVerordnungsEntscheidung())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeantragtePackungsanzahl() != null) {
            _hashCode += getBeantragtePackungsanzahl().hashCode();
        }
        if (getBeantragtesMedikament() != null) {
            _hashCode += getBeantragtesMedikament().hashCode();
        }
        if (getBewilligtePackungsanzahl() != null) {
            _hashCode += getBewilligtePackungsanzahl().hashCode();
        }
        if (getBewilligtesMedikament() != null) {
            _hashCode += getBewilligtesMedikament().hashCode();
        }
        if (getInfoText() != null) {
            _hashCode += getInfoText().hashCode();
        }
        if (getLangzeitVerordnung() != null) {
            _hashCode += getLangzeitVerordnung().hashCode();
        }
        if (getMagistraleZubereitung() != null) {
            _hashCode += getMagistraleZubereitung().hashCode();
        }
        if (getVerordnungsEntscheidung() != null) {
            _hashCode += getVerordnungsEntscheidung().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AnfrageEntscheid.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageEntscheid"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantragtePackungsanzahl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantragtePackungsanzahl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantragtesMedikament");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantragtesMedikament"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikament"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligtePackungsanzahl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligtePackungsanzahl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligtesMedikament");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligtesMedikament"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikament"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "infoText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("langzeitVerordnung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitVerordnung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitVerordnung"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magistraleZubereitung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "magistraleZubereitung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verordnungsEntscheidung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnungsEntscheidung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
