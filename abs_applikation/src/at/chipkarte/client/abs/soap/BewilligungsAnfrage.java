/**
 * BewilligungsAnfrage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class BewilligungsAnfrage  implements java.io.Serializable {
    private java.lang.String antragstyp;

    private at.chipkarte.client.abs.soap.PatientenDaten patientenDaten;

    private java.lang.String verordnerinformation;

    private at.chipkarte.client.abs.soap.Verordnung[] verordnungen;

    public BewilligungsAnfrage() {
    }

    public BewilligungsAnfrage(
           java.lang.String antragstyp,
           at.chipkarte.client.abs.soap.PatientenDaten patientenDaten,
           java.lang.String verordnerinformation,
           at.chipkarte.client.abs.soap.Verordnung[] verordnungen) {
           this.antragstyp = antragstyp;
           this.patientenDaten = patientenDaten;
           this.verordnerinformation = verordnerinformation;
           this.verordnungen = verordnungen;
    }


    /**
     * Gets the antragstyp value for this BewilligungsAnfrage.
     * 
     * @return antragstyp
     */
    public java.lang.String getAntragstyp() {
        return antragstyp;
    }


    /**
     * Sets the antragstyp value for this BewilligungsAnfrage.
     * 
     * @param antragstyp
     */
    public void setAntragstyp(java.lang.String antragstyp) {
        this.antragstyp = antragstyp;
    }


    /**
     * Gets the patientenDaten value for this BewilligungsAnfrage.
     * 
     * @return patientenDaten
     */
    public at.chipkarte.client.abs.soap.PatientenDaten getPatientenDaten() {
        return patientenDaten;
    }


    /**
     * Sets the patientenDaten value for this BewilligungsAnfrage.
     * 
     * @param patientenDaten
     */
    public void setPatientenDaten(at.chipkarte.client.abs.soap.PatientenDaten patientenDaten) {
        this.patientenDaten = patientenDaten;
    }


    /**
     * Gets the verordnerinformation value for this BewilligungsAnfrage.
     * 
     * @return verordnerinformation
     */
    public java.lang.String getVerordnerinformation() {
        return verordnerinformation;
    }


    /**
     * Sets the verordnerinformation value for this BewilligungsAnfrage.
     * 
     * @param verordnerinformation
     */
    public void setVerordnerinformation(java.lang.String verordnerinformation) {
        this.verordnerinformation = verordnerinformation;
    }


    /**
     * Gets the verordnungen value for this BewilligungsAnfrage.
     * 
     * @return verordnungen
     */
    public at.chipkarte.client.abs.soap.Verordnung[] getVerordnungen() {
        return verordnungen;
    }


    /**
     * Sets the verordnungen value for this BewilligungsAnfrage.
     * 
     * @param verordnungen
     */
    public void setVerordnungen(at.chipkarte.client.abs.soap.Verordnung[] verordnungen) {
        this.verordnungen = verordnungen;
    }

    public at.chipkarte.client.abs.soap.Verordnung getVerordnungen(int i) {
        return this.verordnungen[i];
    }

    public void setVerordnungen(int i, at.chipkarte.client.abs.soap.Verordnung _value) {
        this.verordnungen[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BewilligungsAnfrage)) return false;
        BewilligungsAnfrage other = (BewilligungsAnfrage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antragstyp==null && other.getAntragstyp()==null) || 
             (this.antragstyp!=null &&
              this.antragstyp.equals(other.getAntragstyp()))) &&
            ((this.patientenDaten==null && other.getPatientenDaten()==null) || 
             (this.patientenDaten!=null &&
              this.patientenDaten.equals(other.getPatientenDaten()))) &&
            ((this.verordnerinformation==null && other.getVerordnerinformation()==null) || 
             (this.verordnerinformation!=null &&
              this.verordnerinformation.equals(other.getVerordnerinformation()))) &&
            ((this.verordnungen==null && other.getVerordnungen()==null) || 
             (this.verordnungen!=null &&
              java.util.Arrays.equals(this.verordnungen, other.getVerordnungen())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntragstyp() != null) {
            _hashCode += getAntragstyp().hashCode();
        }
        if (getPatientenDaten() != null) {
            _hashCode += getPatientenDaten().hashCode();
        }
        if (getVerordnerinformation() != null) {
            _hashCode += getVerordnerinformation().hashCode();
        }
        if (getVerordnungen() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVerordnungen());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVerordnungen(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BewilligungsAnfrage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antragstyp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "antragstyp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientenDaten");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verordnerinformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnerinformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verordnungen");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnungen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnung"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
