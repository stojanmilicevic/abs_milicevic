/**
 * FilterKriterien.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class FilterKriterien  implements java.io.Serializable {
    private java.lang.String anfrageStatus;

    private java.lang.String datumBis;

    private java.lang.String datumVon;

    public FilterKriterien() {
    }

    public FilterKriterien(
           java.lang.String anfrageStatus,
           java.lang.String datumBis,
           java.lang.String datumVon) {
           this.anfrageStatus = anfrageStatus;
           this.datumBis = datumBis;
           this.datumVon = datumVon;
    }


    /**
     * Gets the anfrageStatus value for this FilterKriterien.
     * 
     * @return anfrageStatus
     */
    public java.lang.String getAnfrageStatus() {
        return anfrageStatus;
    }


    /**
     * Sets the anfrageStatus value for this FilterKriterien.
     * 
     * @param anfrageStatus
     */
    public void setAnfrageStatus(java.lang.String anfrageStatus) {
        this.anfrageStatus = anfrageStatus;
    }


    /**
     * Gets the datumBis value for this FilterKriterien.
     * 
     * @return datumBis
     */
    public java.lang.String getDatumBis() {
        return datumBis;
    }


    /**
     * Sets the datumBis value for this FilterKriterien.
     * 
     * @param datumBis
     */
    public void setDatumBis(java.lang.String datumBis) {
        this.datumBis = datumBis;
    }


    /**
     * Gets the datumVon value for this FilterKriterien.
     * 
     * @return datumVon
     */
    public java.lang.String getDatumVon() {
        return datumVon;
    }


    /**
     * Sets the datumVon value for this FilterKriterien.
     * 
     * @param datumVon
     */
    public void setDatumVon(java.lang.String datumVon) {
        this.datumVon = datumVon;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FilterKriterien)) return false;
        FilterKriterien other = (FilterKriterien) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.anfrageStatus==null && other.getAnfrageStatus()==null) || 
             (this.anfrageStatus!=null &&
              this.anfrageStatus.equals(other.getAnfrageStatus()))) &&
            ((this.datumBis==null && other.getDatumBis()==null) || 
             (this.datumBis!=null &&
              this.datumBis.equals(other.getDatumBis()))) &&
            ((this.datumVon==null && other.getDatumVon()==null) || 
             (this.datumVon!=null &&
              this.datumVon.equals(other.getDatumVon())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAnfrageStatus() != null) {
            _hashCode += getAnfrageStatus().hashCode();
        }
        if (getDatumBis() != null) {
            _hashCode += getDatumBis().hashCode();
        }
        if (getDatumVon() != null) {
            _hashCode += getDatumVon().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FilterKriterien.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "filterKriterien"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anfrageStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datumBis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "datumBis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datumVon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "datumVon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
