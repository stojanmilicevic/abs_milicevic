/**
 * FolgeverordnungsAusstellungsErgebnis.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class FolgeverordnungsAusstellungsErgebnis  implements java.io.Serializable {
    private java.lang.Integer bewilligteAbgabemenge;

    private java.lang.String druckNachnamePatient;

    private java.lang.String druckVornamePatient;

    private java.lang.String medikamentName;

    private java.lang.String nachnamePatient;

    private java.lang.String pharmanummer;

    private java.lang.String referenzNr;

    private java.lang.Boolean rezeptGebBefreit;

    private java.lang.String SVNRPatient;

    private java.lang.String SVTCode;

    private java.lang.String vornamePatient;

    public FolgeverordnungsAusstellungsErgebnis() {
    }

    public FolgeverordnungsAusstellungsErgebnis(
           java.lang.Integer bewilligteAbgabemenge,
           java.lang.String druckNachnamePatient,
           java.lang.String druckVornamePatient,
           java.lang.String medikamentName,
           java.lang.String nachnamePatient,
           java.lang.String pharmanummer,
           java.lang.String referenzNr,
           java.lang.Boolean rezeptGebBefreit,
           java.lang.String SVNRPatient,
           java.lang.String SVTCode,
           java.lang.String vornamePatient) {
           this.bewilligteAbgabemenge = bewilligteAbgabemenge;
           this.druckNachnamePatient = druckNachnamePatient;
           this.druckVornamePatient = druckVornamePatient;
           this.medikamentName = medikamentName;
           this.nachnamePatient = nachnamePatient;
           this.pharmanummer = pharmanummer;
           this.referenzNr = referenzNr;
           this.rezeptGebBefreit = rezeptGebBefreit;
           this.SVNRPatient = SVNRPatient;
           this.SVTCode = SVTCode;
           this.vornamePatient = vornamePatient;
    }


    /**
     * Gets the bewilligteAbgabemenge value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return bewilligteAbgabemenge
     */
    public java.lang.Integer getBewilligteAbgabemenge() {
        return bewilligteAbgabemenge;
    }


    /**
     * Sets the bewilligteAbgabemenge value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param bewilligteAbgabemenge
     */
    public void setBewilligteAbgabemenge(java.lang.Integer bewilligteAbgabemenge) {
        this.bewilligteAbgabemenge = bewilligteAbgabemenge;
    }


    /**
     * Gets the druckNachnamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return druckNachnamePatient
     */
    public java.lang.String getDruckNachnamePatient() {
        return druckNachnamePatient;
    }


    /**
     * Sets the druckNachnamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param druckNachnamePatient
     */
    public void setDruckNachnamePatient(java.lang.String druckNachnamePatient) {
        this.druckNachnamePatient = druckNachnamePatient;
    }


    /**
     * Gets the druckVornamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return druckVornamePatient
     */
    public java.lang.String getDruckVornamePatient() {
        return druckVornamePatient;
    }


    /**
     * Sets the druckVornamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param druckVornamePatient
     */
    public void setDruckVornamePatient(java.lang.String druckVornamePatient) {
        this.druckVornamePatient = druckVornamePatient;
    }


    /**
     * Gets the medikamentName value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return medikamentName
     */
    public java.lang.String getMedikamentName() {
        return medikamentName;
    }


    /**
     * Sets the medikamentName value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param medikamentName
     */
    public void setMedikamentName(java.lang.String medikamentName) {
        this.medikamentName = medikamentName;
    }


    /**
     * Gets the nachnamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return nachnamePatient
     */
    public java.lang.String getNachnamePatient() {
        return nachnamePatient;
    }


    /**
     * Sets the nachnamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param nachnamePatient
     */
    public void setNachnamePatient(java.lang.String nachnamePatient) {
        this.nachnamePatient = nachnamePatient;
    }


    /**
     * Gets the pharmanummer value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return pharmanummer
     */
    public java.lang.String getPharmanummer() {
        return pharmanummer;
    }


    /**
     * Sets the pharmanummer value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param pharmanummer
     */
    public void setPharmanummer(java.lang.String pharmanummer) {
        this.pharmanummer = pharmanummer;
    }


    /**
     * Gets the referenzNr value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return referenzNr
     */
    public java.lang.String getReferenzNr() {
        return referenzNr;
    }


    /**
     * Sets the referenzNr value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param referenzNr
     */
    public void setReferenzNr(java.lang.String referenzNr) {
        this.referenzNr = referenzNr;
    }


    /**
     * Gets the rezeptGebBefreit value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return rezeptGebBefreit
     */
    public java.lang.Boolean getRezeptGebBefreit() {
        return rezeptGebBefreit;
    }


    /**
     * Sets the rezeptGebBefreit value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param rezeptGebBefreit
     */
    public void setRezeptGebBefreit(java.lang.Boolean rezeptGebBefreit) {
        this.rezeptGebBefreit = rezeptGebBefreit;
    }


    /**
     * Gets the SVNRPatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return SVNRPatient
     */
    public java.lang.String getSVNRPatient() {
        return SVNRPatient;
    }


    /**
     * Sets the SVNRPatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param SVNRPatient
     */
    public void setSVNRPatient(java.lang.String SVNRPatient) {
        this.SVNRPatient = SVNRPatient;
    }


    /**
     * Gets the SVTCode value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return SVTCode
     */
    public java.lang.String getSVTCode() {
        return SVTCode;
    }


    /**
     * Sets the SVTCode value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param SVTCode
     */
    public void setSVTCode(java.lang.String SVTCode) {
        this.SVTCode = SVTCode;
    }


    /**
     * Gets the vornamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @return vornamePatient
     */
    public java.lang.String getVornamePatient() {
        return vornamePatient;
    }


    /**
     * Sets the vornamePatient value for this FolgeverordnungsAusstellungsErgebnis.
     * 
     * @param vornamePatient
     */
    public void setVornamePatient(java.lang.String vornamePatient) {
        this.vornamePatient = vornamePatient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FolgeverordnungsAusstellungsErgebnis)) return false;
        FolgeverordnungsAusstellungsErgebnis other = (FolgeverordnungsAusstellungsErgebnis) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bewilligteAbgabemenge==null && other.getBewilligteAbgabemenge()==null) || 
             (this.bewilligteAbgabemenge!=null &&
              this.bewilligteAbgabemenge.equals(other.getBewilligteAbgabemenge()))) &&
            ((this.druckNachnamePatient==null && other.getDruckNachnamePatient()==null) || 
             (this.druckNachnamePatient!=null &&
              this.druckNachnamePatient.equals(other.getDruckNachnamePatient()))) &&
            ((this.druckVornamePatient==null && other.getDruckVornamePatient()==null) || 
             (this.druckVornamePatient!=null &&
              this.druckVornamePatient.equals(other.getDruckVornamePatient()))) &&
            ((this.medikamentName==null && other.getMedikamentName()==null) || 
             (this.medikamentName!=null &&
              this.medikamentName.equals(other.getMedikamentName()))) &&
            ((this.nachnamePatient==null && other.getNachnamePatient()==null) || 
             (this.nachnamePatient!=null &&
              this.nachnamePatient.equals(other.getNachnamePatient()))) &&
            ((this.pharmanummer==null && other.getPharmanummer()==null) || 
             (this.pharmanummer!=null &&
              this.pharmanummer.equals(other.getPharmanummer()))) &&
            ((this.referenzNr==null && other.getReferenzNr()==null) || 
             (this.referenzNr!=null &&
              this.referenzNr.equals(other.getReferenzNr()))) &&
            ((this.rezeptGebBefreit==null && other.getRezeptGebBefreit()==null) || 
             (this.rezeptGebBefreit!=null &&
              this.rezeptGebBefreit.equals(other.getRezeptGebBefreit()))) &&
            ((this.SVNRPatient==null && other.getSVNRPatient()==null) || 
             (this.SVNRPatient!=null &&
              this.SVNRPatient.equals(other.getSVNRPatient()))) &&
            ((this.SVTCode==null && other.getSVTCode()==null) || 
             (this.SVTCode!=null &&
              this.SVTCode.equals(other.getSVTCode()))) &&
            ((this.vornamePatient==null && other.getVornamePatient()==null) || 
             (this.vornamePatient!=null &&
              this.vornamePatient.equals(other.getVornamePatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBewilligteAbgabemenge() != null) {
            _hashCode += getBewilligteAbgabemenge().hashCode();
        }
        if (getDruckNachnamePatient() != null) {
            _hashCode += getDruckNachnamePatient().hashCode();
        }
        if (getDruckVornamePatient() != null) {
            _hashCode += getDruckVornamePatient().hashCode();
        }
        if (getMedikamentName() != null) {
            _hashCode += getMedikamentName().hashCode();
        }
        if (getNachnamePatient() != null) {
            _hashCode += getNachnamePatient().hashCode();
        }
        if (getPharmanummer() != null) {
            _hashCode += getPharmanummer().hashCode();
        }
        if (getReferenzNr() != null) {
            _hashCode += getReferenzNr().hashCode();
        }
        if (getRezeptGebBefreit() != null) {
            _hashCode += getRezeptGebBefreit().hashCode();
        }
        if (getSVNRPatient() != null) {
            _hashCode += getSVNRPatient().hashCode();
        }
        if (getSVTCode() != null) {
            _hashCode += getSVTCode().hashCode();
        }
        if (getVornamePatient() != null) {
            _hashCode += getVornamePatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FolgeverordnungsAusstellungsErgebnis.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsErgebnis"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligteAbgabemenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligteAbgabemenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckNachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckNachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckVornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckVornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medikamentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikamentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmanummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "pharmanummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenzNr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "referenzNr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rezeptGebBefreit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "rezeptGebBefreit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNRPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNRPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVTCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVTCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
