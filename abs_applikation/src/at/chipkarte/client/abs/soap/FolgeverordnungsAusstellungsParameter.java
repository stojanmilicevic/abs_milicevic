/**
 * FolgeverordnungsAusstellungsParameter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class FolgeverordnungsAusstellungsParameter  implements java.io.Serializable {
    private java.lang.String SVNRPatient;

    private java.lang.String SVTCode;

    private java.lang.Integer beantragteAbgabemenge;

    private java.lang.String medikamentName;

    private java.lang.String pharmanummer;

    public FolgeverordnungsAusstellungsParameter() {
    }

    public FolgeverordnungsAusstellungsParameter(
           java.lang.String SVNRPatient,
           java.lang.String SVTCode,
           java.lang.Integer beantragteAbgabemenge,
           java.lang.String medikamentName,
           java.lang.String pharmanummer) {
           this.SVNRPatient = SVNRPatient;
           this.SVTCode = SVTCode;
           this.beantragteAbgabemenge = beantragteAbgabemenge;
           this.medikamentName = medikamentName;
           this.pharmanummer = pharmanummer;
    }


    /**
     * Gets the SVNRPatient value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @return SVNRPatient
     */
    public java.lang.String getSVNRPatient() {
        return SVNRPatient;
    }


    /**
     * Sets the SVNRPatient value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @param SVNRPatient
     */
    public void setSVNRPatient(java.lang.String SVNRPatient) {
        this.SVNRPatient = SVNRPatient;
    }


    /**
     * Gets the SVTCode value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @return SVTCode
     */
    public java.lang.String getSVTCode() {
        return SVTCode;
    }


    /**
     * Sets the SVTCode value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @param SVTCode
     */
    public void setSVTCode(java.lang.String SVTCode) {
        this.SVTCode = SVTCode;
    }


    /**
     * Gets the beantragteAbgabemenge value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @return beantragteAbgabemenge
     */
    public java.lang.Integer getBeantragteAbgabemenge() {
        return beantragteAbgabemenge;
    }


    /**
     * Sets the beantragteAbgabemenge value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @param beantragteAbgabemenge
     */
    public void setBeantragteAbgabemenge(java.lang.Integer beantragteAbgabemenge) {
        this.beantragteAbgabemenge = beantragteAbgabemenge;
    }


    /**
     * Gets the medikamentName value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @return medikamentName
     */
    public java.lang.String getMedikamentName() {
        return medikamentName;
    }


    /**
     * Sets the medikamentName value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @param medikamentName
     */
    public void setMedikamentName(java.lang.String medikamentName) {
        this.medikamentName = medikamentName;
    }


    /**
     * Gets the pharmanummer value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @return pharmanummer
     */
    public java.lang.String getPharmanummer() {
        return pharmanummer;
    }


    /**
     * Sets the pharmanummer value for this FolgeverordnungsAusstellungsParameter.
     * 
     * @param pharmanummer
     */
    public void setPharmanummer(java.lang.String pharmanummer) {
        this.pharmanummer = pharmanummer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FolgeverordnungsAusstellungsParameter)) return false;
        FolgeverordnungsAusstellungsParameter other = (FolgeverordnungsAusstellungsParameter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SVNRPatient==null && other.getSVNRPatient()==null) || 
             (this.SVNRPatient!=null &&
              this.SVNRPatient.equals(other.getSVNRPatient()))) &&
            ((this.SVTCode==null && other.getSVTCode()==null) || 
             (this.SVTCode!=null &&
              this.SVTCode.equals(other.getSVTCode()))) &&
            ((this.beantragteAbgabemenge==null && other.getBeantragteAbgabemenge()==null) || 
             (this.beantragteAbgabemenge!=null &&
              this.beantragteAbgabemenge.equals(other.getBeantragteAbgabemenge()))) &&
            ((this.medikamentName==null && other.getMedikamentName()==null) || 
             (this.medikamentName!=null &&
              this.medikamentName.equals(other.getMedikamentName()))) &&
            ((this.pharmanummer==null && other.getPharmanummer()==null) || 
             (this.pharmanummer!=null &&
              this.pharmanummer.equals(other.getPharmanummer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSVNRPatient() != null) {
            _hashCode += getSVNRPatient().hashCode();
        }
        if (getSVTCode() != null) {
            _hashCode += getSVTCode().hashCode();
        }
        if (getBeantragteAbgabemenge() != null) {
            _hashCode += getBeantragteAbgabemenge().hashCode();
        }
        if (getMedikamentName() != null) {
            _hashCode += getMedikamentName().hashCode();
        }
        if (getPharmanummer() != null) {
            _hashCode += getPharmanummer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FolgeverordnungsAusstellungsParameter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "folgeverordnungsAusstellungsParameter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNRPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNRPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVTCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVTCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantragteAbgabemenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantragteAbgabemenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medikamentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikamentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmanummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "pharmanummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
