package at.chipkarte.client.abs.soap;

public class IAbsServiceProxy implements at.chipkarte.client.abs.soap.IAbsService {
  private String _endpoint = null;
  private at.chipkarte.client.abs.soap.IAbsService iAbsService = null;
  
  public IAbsServiceProxy() {
    _initIAbsServiceProxy();
  }
  
  public IAbsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIAbsServiceProxy();
  }
  
  private void _initIAbsServiceProxy() {
    try {
      iAbsService = (new at.chipkarte.client.abs.soap.AbsServiceLocator()).getabs_11();
      if (iAbsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iAbsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iAbsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iAbsService != null)
      ((javax.xml.rpc.Stub)iAbsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public at.chipkarte.client.abs.soap.IAbsService getIAbsService() {
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService;
  }
  
  public at.chipkarte.client.abs.soap.LangzeitbewilligungsAbfrageErgebnis[] abfragenLangzeitbewilligung(java.lang.String dialogId, java.lang.String sVNRPatient, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.abfragenLangzeitbewilligung(dialogId, sVNRPatient, cardReaderId);
  }
  
  public at.chipkarte.client.base.soap.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.checkStatus(dialogId);
  }
  
  public at.chipkarte.client.abs.soap.BewilligungsAnfrage ermittelnPatientenDaten(java.lang.String dialogId, java.lang.String svNummer, java.lang.String svtCode, java.lang.String ekvkNummer, java.lang.String geschlecht, java.lang.String nachname, java.lang.String vorname, java.lang.String antragstyp, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.ermittelnPatientenDaten(dialogId, svNummer, svtCode, ekvkNummer, geschlecht, nachname, vorname, antragstyp, cardReaderId);
  }
  
  public at.chipkarte.client.abs.soap.Rueckantwort getRueckantwort(java.lang.String dialogId, java.lang.String anfrageId) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterRueckantwortExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.getRueckantwort(dialogId, anfrageId);
  }
  
  public at.chipkarte.client.base.soap.SvtProperty[] getSVTs() throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.getSVTs();
  }
  
  public at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[] getStatusBewilligungsAnfragen(java.lang.String dialogId, at.chipkarte.client.abs.soap.FilterKriterien filterkriterien) throws java.rmi.RemoteException, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.getStatusBewilligungsAnfragen(dialogId, filterkriterien);
  }
  
  public at.chipkarte.client.abs.soap.StatusBewilligungsAnfrage[] getStatusPatientBewilligungsAnfragen(java.lang.String dialogId, java.lang.String svNummer, java.lang.String nachname, java.lang.String vorname, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.abs.soap.exceptions.InvalidParameterGetAnfrageStatusExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.getStatusPatientBewilligungsAnfragen(dialogId, svNummer, nachname, vorname, cardReaderId);
  }
  
  public at.chipkarte.client.abs.soap.AnfrageAntwort sendenAnfrage(java.lang.String dialogId, at.chipkarte.client.abs.soap.BewilligungsAnfrage anfrage, java.lang.String cardReaderId, byte[] attachment) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.sendenAnfrage(dialogId, anfrage, cardReaderId, attachment);
  }
  
  public at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsErgebnis sendenFolgeverordnung(java.lang.String dialogId, at.chipkarte.client.abs.soap.FolgeverordnungsAusstellungsParameter ausstellungsParameter, java.lang.String cardReaderId) throws java.rmi.RemoteException, at.chipkarte.client.abs.soap.exceptions.InvalidParameterBewilligungsanfrageExceptionContent, at.chipkarte.client.base.soap.exceptions.AccessExceptionContent, at.chipkarte.client.base.soap.exceptions.ServiceExceptionContent, at.chipkarte.client.base.soap.exceptions.DialogExceptionContent, at.chipkarte.client.abs.soap.exceptions.AbsExceptionContent, at.chipkarte.client.base.soap.exceptions.CardExceptionContent{
    if (iAbsService == null)
      _initIAbsServiceProxy();
    return iAbsService.sendenFolgeverordnung(dialogId, ausstellungsParameter, cardReaderId);
  }
  
  
}