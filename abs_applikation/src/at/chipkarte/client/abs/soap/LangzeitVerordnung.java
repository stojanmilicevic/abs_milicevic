/**
 * LangzeitVerordnung.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class LangzeitVerordnung  implements java.io.Serializable {
    private java.lang.Integer beantragteMonate;

    private java.lang.Integer bewilligteMonate;

    private java.lang.Integer bewilligtePackungen;

    public LangzeitVerordnung() {
    }

    public LangzeitVerordnung(
           java.lang.Integer beantragteMonate,
           java.lang.Integer bewilligteMonate,
           java.lang.Integer bewilligtePackungen) {
           this.beantragteMonate = beantragteMonate;
           this.bewilligteMonate = bewilligteMonate;
           this.bewilligtePackungen = bewilligtePackungen;
    }


    /**
     * Gets the beantragteMonate value for this LangzeitVerordnung.
     * 
     * @return beantragteMonate
     */
    public java.lang.Integer getBeantragteMonate() {
        return beantragteMonate;
    }


    /**
     * Sets the beantragteMonate value for this LangzeitVerordnung.
     * 
     * @param beantragteMonate
     */
    public void setBeantragteMonate(java.lang.Integer beantragteMonate) {
        this.beantragteMonate = beantragteMonate;
    }


    /**
     * Gets the bewilligteMonate value for this LangzeitVerordnung.
     * 
     * @return bewilligteMonate
     */
    public java.lang.Integer getBewilligteMonate() {
        return bewilligteMonate;
    }


    /**
     * Sets the bewilligteMonate value for this LangzeitVerordnung.
     * 
     * @param bewilligteMonate
     */
    public void setBewilligteMonate(java.lang.Integer bewilligteMonate) {
        this.bewilligteMonate = bewilligteMonate;
    }


    /**
     * Gets the bewilligtePackungen value for this LangzeitVerordnung.
     * 
     * @return bewilligtePackungen
     */
    public java.lang.Integer getBewilligtePackungen() {
        return bewilligtePackungen;
    }


    /**
     * Sets the bewilligtePackungen value for this LangzeitVerordnung.
     * 
     * @param bewilligtePackungen
     */
    public void setBewilligtePackungen(java.lang.Integer bewilligtePackungen) {
        this.bewilligtePackungen = bewilligtePackungen;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LangzeitVerordnung)) return false;
        LangzeitVerordnung other = (LangzeitVerordnung) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beantragteMonate==null && other.getBeantragteMonate()==null) || 
             (this.beantragteMonate!=null &&
              this.beantragteMonate.equals(other.getBeantragteMonate()))) &&
            ((this.bewilligteMonate==null && other.getBewilligteMonate()==null) || 
             (this.bewilligteMonate!=null &&
              this.bewilligteMonate.equals(other.getBewilligteMonate()))) &&
            ((this.bewilligtePackungen==null && other.getBewilligtePackungen()==null) || 
             (this.bewilligtePackungen!=null &&
              this.bewilligtePackungen.equals(other.getBewilligtePackungen())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeantragteMonate() != null) {
            _hashCode += getBeantragteMonate().hashCode();
        }
        if (getBewilligteMonate() != null) {
            _hashCode += getBewilligteMonate().hashCode();
        }
        if (getBewilligtePackungen() != null) {
            _hashCode += getBewilligtePackungen().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LangzeitVerordnung.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitVerordnung"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantragteMonate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantragteMonate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligteMonate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligteMonate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligtePackungen");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligtePackungen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
