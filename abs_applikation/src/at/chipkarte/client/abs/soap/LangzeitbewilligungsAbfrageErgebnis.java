/**
 * LangzeitbewilligungsAbfrageErgebnis.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class LangzeitbewilligungsAbfrageErgebnis  implements java.io.Serializable {
    private java.lang.Boolean abgabeMoeglich;

    private java.lang.Integer bewilligteAbgabemenge;

    private java.lang.String datumGueltigBis;

    private java.lang.String datumNaechstMoeglicheFolgeverordnung;

    private java.lang.String dosierung;

    private java.lang.String druckNachnamePatient;

    private java.lang.String druckVornamePatient;

    private java.lang.Boolean heuteBereitsVerordnet;

    private java.lang.Integer heuteBereitsVerordneteMenge;

    private java.lang.Integer maximaleAbgabemenge;

    private java.lang.String medikamentName;

    private java.lang.Integer monatsBedarf;

    private java.lang.String nachnamePatient;

    private java.lang.Integer offeneAbgabemenge;

    private java.lang.String pharmanummer;

    private java.lang.String SVNRPatient;

    private java.lang.String vornamePatient;

    public LangzeitbewilligungsAbfrageErgebnis() {
    }

    public LangzeitbewilligungsAbfrageErgebnis(
           java.lang.Boolean abgabeMoeglich,
           java.lang.Integer bewilligteAbgabemenge,
           java.lang.String datumGueltigBis,
           java.lang.String datumNaechstMoeglicheFolgeverordnung,
           java.lang.String dosierung,
           java.lang.String druckNachnamePatient,
           java.lang.String druckVornamePatient,
           java.lang.Boolean heuteBereitsVerordnet,
           java.lang.Integer heuteBereitsVerordneteMenge,
           java.lang.Integer maximaleAbgabemenge,
           java.lang.String medikamentName,
           java.lang.Integer monatsBedarf,
           java.lang.String nachnamePatient,
           java.lang.Integer offeneAbgabemenge,
           java.lang.String pharmanummer,
           java.lang.String SVNRPatient,
           java.lang.String vornamePatient) {
           this.abgabeMoeglich = abgabeMoeglich;
           this.bewilligteAbgabemenge = bewilligteAbgabemenge;
           this.datumGueltigBis = datumGueltigBis;
           this.datumNaechstMoeglicheFolgeverordnung = datumNaechstMoeglicheFolgeverordnung;
           this.dosierung = dosierung;
           this.druckNachnamePatient = druckNachnamePatient;
           this.druckVornamePatient = druckVornamePatient;
           this.heuteBereitsVerordnet = heuteBereitsVerordnet;
           this.heuteBereitsVerordneteMenge = heuteBereitsVerordneteMenge;
           this.maximaleAbgabemenge = maximaleAbgabemenge;
           this.medikamentName = medikamentName;
           this.monatsBedarf = monatsBedarf;
           this.nachnamePatient = nachnamePatient;
           this.offeneAbgabemenge = offeneAbgabemenge;
           this.pharmanummer = pharmanummer;
           this.SVNRPatient = SVNRPatient;
           this.vornamePatient = vornamePatient;
    }


    /**
     * Gets the abgabeMoeglich value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return abgabeMoeglich
     */
    public java.lang.Boolean getAbgabeMoeglich() {
        return abgabeMoeglich;
    }


    /**
     * Sets the abgabeMoeglich value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param abgabeMoeglich
     */
    public void setAbgabeMoeglich(java.lang.Boolean abgabeMoeglich) {
        this.abgabeMoeglich = abgabeMoeglich;
    }


    /**
     * Gets the bewilligteAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return bewilligteAbgabemenge
     */
    public java.lang.Integer getBewilligteAbgabemenge() {
        return bewilligteAbgabemenge;
    }


    /**
     * Sets the bewilligteAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param bewilligteAbgabemenge
     */
    public void setBewilligteAbgabemenge(java.lang.Integer bewilligteAbgabemenge) {
        this.bewilligteAbgabemenge = bewilligteAbgabemenge;
    }


    /**
     * Gets the datumGueltigBis value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return datumGueltigBis
     */
    public java.lang.String getDatumGueltigBis() {
        return datumGueltigBis;
    }


    /**
     * Sets the datumGueltigBis value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param datumGueltigBis
     */
    public void setDatumGueltigBis(java.lang.String datumGueltigBis) {
        this.datumGueltigBis = datumGueltigBis;
    }


    /**
     * Gets the datumNaechstMoeglicheFolgeverordnung value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return datumNaechstMoeglicheFolgeverordnung
     */
    public java.lang.String getDatumNaechstMoeglicheFolgeverordnung() {
        return datumNaechstMoeglicheFolgeverordnung;
    }


    /**
     * Sets the datumNaechstMoeglicheFolgeverordnung value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param datumNaechstMoeglicheFolgeverordnung
     */
    public void setDatumNaechstMoeglicheFolgeverordnung(java.lang.String datumNaechstMoeglicheFolgeverordnung) {
        this.datumNaechstMoeglicheFolgeverordnung = datumNaechstMoeglicheFolgeverordnung;
    }


    /**
     * Gets the dosierung value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return dosierung
     */
    public java.lang.String getDosierung() {
        return dosierung;
    }


    /**
     * Sets the dosierung value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param dosierung
     */
    public void setDosierung(java.lang.String dosierung) {
        this.dosierung = dosierung;
    }


    /**
     * Gets the druckNachnamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return druckNachnamePatient
     */
    public java.lang.String getDruckNachnamePatient() {
        return druckNachnamePatient;
    }


    /**
     * Sets the druckNachnamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param druckNachnamePatient
     */
    public void setDruckNachnamePatient(java.lang.String druckNachnamePatient) {
        this.druckNachnamePatient = druckNachnamePatient;
    }


    /**
     * Gets the druckVornamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return druckVornamePatient
     */
    public java.lang.String getDruckVornamePatient() {
        return druckVornamePatient;
    }


    /**
     * Sets the druckVornamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param druckVornamePatient
     */
    public void setDruckVornamePatient(java.lang.String druckVornamePatient) {
        this.druckVornamePatient = druckVornamePatient;
    }


    /**
     * Gets the heuteBereitsVerordnet value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return heuteBereitsVerordnet
     */
    public java.lang.Boolean getHeuteBereitsVerordnet() {
        return heuteBereitsVerordnet;
    }


    /**
     * Sets the heuteBereitsVerordnet value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param heuteBereitsVerordnet
     */
    public void setHeuteBereitsVerordnet(java.lang.Boolean heuteBereitsVerordnet) {
        this.heuteBereitsVerordnet = heuteBereitsVerordnet;
    }


    /**
     * Gets the heuteBereitsVerordneteMenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return heuteBereitsVerordneteMenge
     */
    public java.lang.Integer getHeuteBereitsVerordneteMenge() {
        return heuteBereitsVerordneteMenge;
    }


    /**
     * Sets the heuteBereitsVerordneteMenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param heuteBereitsVerordneteMenge
     */
    public void setHeuteBereitsVerordneteMenge(java.lang.Integer heuteBereitsVerordneteMenge) {
        this.heuteBereitsVerordneteMenge = heuteBereitsVerordneteMenge;
    }


    /**
     * Gets the maximaleAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return maximaleAbgabemenge
     */
    public java.lang.Integer getMaximaleAbgabemenge() {
        return maximaleAbgabemenge;
    }


    /**
     * Sets the maximaleAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param maximaleAbgabemenge
     */
    public void setMaximaleAbgabemenge(java.lang.Integer maximaleAbgabemenge) {
        this.maximaleAbgabemenge = maximaleAbgabemenge;
    }


    /**
     * Gets the medikamentName value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return medikamentName
     */
    public java.lang.String getMedikamentName() {
        return medikamentName;
    }


    /**
     * Sets the medikamentName value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param medikamentName
     */
    public void setMedikamentName(java.lang.String medikamentName) {
        this.medikamentName = medikamentName;
    }


    /**
     * Gets the monatsBedarf value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return monatsBedarf
     */
    public java.lang.Integer getMonatsBedarf() {
        return monatsBedarf;
    }


    /**
     * Sets the monatsBedarf value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param monatsBedarf
     */
    public void setMonatsBedarf(java.lang.Integer monatsBedarf) {
        this.monatsBedarf = monatsBedarf;
    }


    /**
     * Gets the nachnamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return nachnamePatient
     */
    public java.lang.String getNachnamePatient() {
        return nachnamePatient;
    }


    /**
     * Sets the nachnamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param nachnamePatient
     */
    public void setNachnamePatient(java.lang.String nachnamePatient) {
        this.nachnamePatient = nachnamePatient;
    }


    /**
     * Gets the offeneAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return offeneAbgabemenge
     */
    public java.lang.Integer getOffeneAbgabemenge() {
        return offeneAbgabemenge;
    }


    /**
     * Sets the offeneAbgabemenge value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param offeneAbgabemenge
     */
    public void setOffeneAbgabemenge(java.lang.Integer offeneAbgabemenge) {
        this.offeneAbgabemenge = offeneAbgabemenge;
    }


    /**
     * Gets the pharmanummer value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return pharmanummer
     */
    public java.lang.String getPharmanummer() {
        return pharmanummer;
    }


    /**
     * Sets the pharmanummer value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param pharmanummer
     */
    public void setPharmanummer(java.lang.String pharmanummer) {
        this.pharmanummer = pharmanummer;
    }


    /**
     * Gets the SVNRPatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return SVNRPatient
     */
    public java.lang.String getSVNRPatient() {
        return SVNRPatient;
    }


    /**
     * Sets the SVNRPatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param SVNRPatient
     */
    public void setSVNRPatient(java.lang.String SVNRPatient) {
        this.SVNRPatient = SVNRPatient;
    }


    /**
     * Gets the vornamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @return vornamePatient
     */
    public java.lang.String getVornamePatient() {
        return vornamePatient;
    }


    /**
     * Sets the vornamePatient value for this LangzeitbewilligungsAbfrageErgebnis.
     * 
     * @param vornamePatient
     */
    public void setVornamePatient(java.lang.String vornamePatient) {
        this.vornamePatient = vornamePatient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LangzeitbewilligungsAbfrageErgebnis)) return false;
        LangzeitbewilligungsAbfrageErgebnis other = (LangzeitbewilligungsAbfrageErgebnis) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abgabeMoeglich==null && other.getAbgabeMoeglich()==null) || 
             (this.abgabeMoeglich!=null &&
              this.abgabeMoeglich.equals(other.getAbgabeMoeglich()))) &&
            ((this.bewilligteAbgabemenge==null && other.getBewilligteAbgabemenge()==null) || 
             (this.bewilligteAbgabemenge!=null &&
              this.bewilligteAbgabemenge.equals(other.getBewilligteAbgabemenge()))) &&
            ((this.datumGueltigBis==null && other.getDatumGueltigBis()==null) || 
             (this.datumGueltigBis!=null &&
              this.datumGueltigBis.equals(other.getDatumGueltigBis()))) &&
            ((this.datumNaechstMoeglicheFolgeverordnung==null && other.getDatumNaechstMoeglicheFolgeverordnung()==null) || 
             (this.datumNaechstMoeglicheFolgeverordnung!=null &&
              this.datumNaechstMoeglicheFolgeverordnung.equals(other.getDatumNaechstMoeglicheFolgeverordnung()))) &&
            ((this.dosierung==null && other.getDosierung()==null) || 
             (this.dosierung!=null &&
              this.dosierung.equals(other.getDosierung()))) &&
            ((this.druckNachnamePatient==null && other.getDruckNachnamePatient()==null) || 
             (this.druckNachnamePatient!=null &&
              this.druckNachnamePatient.equals(other.getDruckNachnamePatient()))) &&
            ((this.druckVornamePatient==null && other.getDruckVornamePatient()==null) || 
             (this.druckVornamePatient!=null &&
              this.druckVornamePatient.equals(other.getDruckVornamePatient()))) &&
            ((this.heuteBereitsVerordnet==null && other.getHeuteBereitsVerordnet()==null) || 
             (this.heuteBereitsVerordnet!=null &&
              this.heuteBereitsVerordnet.equals(other.getHeuteBereitsVerordnet()))) &&
            ((this.heuteBereitsVerordneteMenge==null && other.getHeuteBereitsVerordneteMenge()==null) || 
             (this.heuteBereitsVerordneteMenge!=null &&
              this.heuteBereitsVerordneteMenge.equals(other.getHeuteBereitsVerordneteMenge()))) &&
            ((this.maximaleAbgabemenge==null && other.getMaximaleAbgabemenge()==null) || 
             (this.maximaleAbgabemenge!=null &&
              this.maximaleAbgabemenge.equals(other.getMaximaleAbgabemenge()))) &&
            ((this.medikamentName==null && other.getMedikamentName()==null) || 
             (this.medikamentName!=null &&
              this.medikamentName.equals(other.getMedikamentName()))) &&
            ((this.monatsBedarf==null && other.getMonatsBedarf()==null) || 
             (this.monatsBedarf!=null &&
              this.monatsBedarf.equals(other.getMonatsBedarf()))) &&
            ((this.nachnamePatient==null && other.getNachnamePatient()==null) || 
             (this.nachnamePatient!=null &&
              this.nachnamePatient.equals(other.getNachnamePatient()))) &&
            ((this.offeneAbgabemenge==null && other.getOffeneAbgabemenge()==null) || 
             (this.offeneAbgabemenge!=null &&
              this.offeneAbgabemenge.equals(other.getOffeneAbgabemenge()))) &&
            ((this.pharmanummer==null && other.getPharmanummer()==null) || 
             (this.pharmanummer!=null &&
              this.pharmanummer.equals(other.getPharmanummer()))) &&
            ((this.SVNRPatient==null && other.getSVNRPatient()==null) || 
             (this.SVNRPatient!=null &&
              this.SVNRPatient.equals(other.getSVNRPatient()))) &&
            ((this.vornamePatient==null && other.getVornamePatient()==null) || 
             (this.vornamePatient!=null &&
              this.vornamePatient.equals(other.getVornamePatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbgabeMoeglich() != null) {
            _hashCode += getAbgabeMoeglich().hashCode();
        }
        if (getBewilligteAbgabemenge() != null) {
            _hashCode += getBewilligteAbgabemenge().hashCode();
        }
        if (getDatumGueltigBis() != null) {
            _hashCode += getDatumGueltigBis().hashCode();
        }
        if (getDatumNaechstMoeglicheFolgeverordnung() != null) {
            _hashCode += getDatumNaechstMoeglicheFolgeverordnung().hashCode();
        }
        if (getDosierung() != null) {
            _hashCode += getDosierung().hashCode();
        }
        if (getDruckNachnamePatient() != null) {
            _hashCode += getDruckNachnamePatient().hashCode();
        }
        if (getDruckVornamePatient() != null) {
            _hashCode += getDruckVornamePatient().hashCode();
        }
        if (getHeuteBereitsVerordnet() != null) {
            _hashCode += getHeuteBereitsVerordnet().hashCode();
        }
        if (getHeuteBereitsVerordneteMenge() != null) {
            _hashCode += getHeuteBereitsVerordneteMenge().hashCode();
        }
        if (getMaximaleAbgabemenge() != null) {
            _hashCode += getMaximaleAbgabemenge().hashCode();
        }
        if (getMedikamentName() != null) {
            _hashCode += getMedikamentName().hashCode();
        }
        if (getMonatsBedarf() != null) {
            _hashCode += getMonatsBedarf().hashCode();
        }
        if (getNachnamePatient() != null) {
            _hashCode += getNachnamePatient().hashCode();
        }
        if (getOffeneAbgabemenge() != null) {
            _hashCode += getOffeneAbgabemenge().hashCode();
        }
        if (getPharmanummer() != null) {
            _hashCode += getPharmanummer().hashCode();
        }
        if (getSVNRPatient() != null) {
            _hashCode += getSVNRPatient().hashCode();
        }
        if (getVornamePatient() != null) {
            _hashCode += getVornamePatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LangzeitbewilligungsAbfrageErgebnis.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitbewilligungsAbfrageErgebnis"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abgabeMoeglich");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "abgabeMoeglich"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligteAbgabemenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligteAbgabemenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datumGueltigBis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "datumGueltigBis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datumNaechstMoeglicheFolgeverordnung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "datumNaechstMoeglicheFolgeverordnung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dosierung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dosierung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckNachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckNachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckVornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckVornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("heuteBereitsVerordnet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "heuteBereitsVerordnet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("heuteBereitsVerordneteMenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "heuteBereitsVerordneteMenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximaleAbgabemenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "maximaleAbgabemenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medikamentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikamentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monatsBedarf");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "monatsBedarf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offeneAbgabemenge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "offeneAbgabemenge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmanummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "pharmanummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNRPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNRPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
