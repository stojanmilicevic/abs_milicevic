/**
 * PatientenDaten.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class PatientenDaten  implements java.io.Serializable {
    private java.lang.String druckNachname;

    private java.lang.String druckVorname;

    private java.lang.String EKVKNummer;

    private java.lang.String geschlecht;

    private java.lang.String nachname;

    private java.lang.Boolean rezeptGebBefreit;

    private java.lang.String SVNummer;

    private java.lang.String SVTCode;

    private java.lang.String vorname;

    public PatientenDaten() {
    }

    public PatientenDaten(
           java.lang.String druckNachname,
           java.lang.String druckVorname,
           java.lang.String EKVKNummer,
           java.lang.String geschlecht,
           java.lang.String nachname,
           java.lang.Boolean rezeptGebBefreit,
           java.lang.String SVNummer,
           java.lang.String SVTCode,
           java.lang.String vorname) {
           this.druckNachname = druckNachname;
           this.druckVorname = druckVorname;
           this.EKVKNummer = EKVKNummer;
           this.geschlecht = geschlecht;
           this.nachname = nachname;
           this.rezeptGebBefreit = rezeptGebBefreit;
           this.SVNummer = SVNummer;
           this.SVTCode = SVTCode;
           this.vorname = vorname;
    }


    /**
     * Gets the druckNachname value for this PatientenDaten.
     * 
     * @return druckNachname
     */
    public java.lang.String getDruckNachname() {
        return druckNachname;
    }


    /**
     * Sets the druckNachname value for this PatientenDaten.
     * 
     * @param druckNachname
     */
    public void setDruckNachname(java.lang.String druckNachname) {
        this.druckNachname = druckNachname;
    }


    /**
     * Gets the druckVorname value for this PatientenDaten.
     * 
     * @return druckVorname
     */
    public java.lang.String getDruckVorname() {
        return druckVorname;
    }


    /**
     * Sets the druckVorname value for this PatientenDaten.
     * 
     * @param druckVorname
     */
    public void setDruckVorname(java.lang.String druckVorname) {
        this.druckVorname = druckVorname;
    }


    /**
     * Gets the EKVKNummer value for this PatientenDaten.
     * 
     * @return EKVKNummer
     */
    public java.lang.String getEKVKNummer() {
        return EKVKNummer;
    }


    /**
     * Sets the EKVKNummer value for this PatientenDaten.
     * 
     * @param EKVKNummer
     */
    public void setEKVKNummer(java.lang.String EKVKNummer) {
        this.EKVKNummer = EKVKNummer;
    }


    /**
     * Gets the geschlecht value for this PatientenDaten.
     * 
     * @return geschlecht
     */
    public java.lang.String getGeschlecht() {
        return geschlecht;
    }


    /**
     * Sets the geschlecht value for this PatientenDaten.
     * 
     * @param geschlecht
     */
    public void setGeschlecht(java.lang.String geschlecht) {
        this.geschlecht = geschlecht;
    }


    /**
     * Gets the nachname value for this PatientenDaten.
     * 
     * @return nachname
     */
    public java.lang.String getNachname() {
        return nachname;
    }


    /**
     * Sets the nachname value for this PatientenDaten.
     * 
     * @param nachname
     */
    public void setNachname(java.lang.String nachname) {
        this.nachname = nachname;
    }


    /**
     * Gets the rezeptGebBefreit value for this PatientenDaten.
     * 
     * @return rezeptGebBefreit
     */
    public java.lang.Boolean getRezeptGebBefreit() {
        return rezeptGebBefreit;
    }


    /**
     * Sets the rezeptGebBefreit value for this PatientenDaten.
     * 
     * @param rezeptGebBefreit
     */
    public void setRezeptGebBefreit(java.lang.Boolean rezeptGebBefreit) {
        this.rezeptGebBefreit = rezeptGebBefreit;
    }


    /**
     * Gets the SVNummer value for this PatientenDaten.
     * 
     * @return SVNummer
     */
    public java.lang.String getSVNummer() {
        return SVNummer;
    }


    /**
     * Sets the SVNummer value for this PatientenDaten.
     * 
     * @param SVNummer
     */
    public void setSVNummer(java.lang.String SVNummer) {
        this.SVNummer = SVNummer;
    }


    /**
     * Gets the SVTCode value for this PatientenDaten.
     * 
     * @return SVTCode
     */
    public java.lang.String getSVTCode() {
        return SVTCode;
    }


    /**
     * Sets the SVTCode value for this PatientenDaten.
     * 
     * @param SVTCode
     */
    public void setSVTCode(java.lang.String SVTCode) {
        this.SVTCode = SVTCode;
    }


    /**
     * Gets the vorname value for this PatientenDaten.
     * 
     * @return vorname
     */
    public java.lang.String getVorname() {
        return vorname;
    }


    /**
     * Sets the vorname value for this PatientenDaten.
     * 
     * @param vorname
     */
    public void setVorname(java.lang.String vorname) {
        this.vorname = vorname;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PatientenDaten)) return false;
        PatientenDaten other = (PatientenDaten) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.druckNachname==null && other.getDruckNachname()==null) || 
             (this.druckNachname!=null &&
              this.druckNachname.equals(other.getDruckNachname()))) &&
            ((this.druckVorname==null && other.getDruckVorname()==null) || 
             (this.druckVorname!=null &&
              this.druckVorname.equals(other.getDruckVorname()))) &&
            ((this.EKVKNummer==null && other.getEKVKNummer()==null) || 
             (this.EKVKNummer!=null &&
              this.EKVKNummer.equals(other.getEKVKNummer()))) &&
            ((this.geschlecht==null && other.getGeschlecht()==null) || 
             (this.geschlecht!=null &&
              this.geschlecht.equals(other.getGeschlecht()))) &&
            ((this.nachname==null && other.getNachname()==null) || 
             (this.nachname!=null &&
              this.nachname.equals(other.getNachname()))) &&
            ((this.rezeptGebBefreit==null && other.getRezeptGebBefreit()==null) || 
             (this.rezeptGebBefreit!=null &&
              this.rezeptGebBefreit.equals(other.getRezeptGebBefreit()))) &&
            ((this.SVNummer==null && other.getSVNummer()==null) || 
             (this.SVNummer!=null &&
              this.SVNummer.equals(other.getSVNummer()))) &&
            ((this.SVTCode==null && other.getSVTCode()==null) || 
             (this.SVTCode!=null &&
              this.SVTCode.equals(other.getSVTCode()))) &&
            ((this.vorname==null && other.getVorname()==null) || 
             (this.vorname!=null &&
              this.vorname.equals(other.getVorname())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDruckNachname() != null) {
            _hashCode += getDruckNachname().hashCode();
        }
        if (getDruckVorname() != null) {
            _hashCode += getDruckVorname().hashCode();
        }
        if (getEKVKNummer() != null) {
            _hashCode += getEKVKNummer().hashCode();
        }
        if (getGeschlecht() != null) {
            _hashCode += getGeschlecht().hashCode();
        }
        if (getNachname() != null) {
            _hashCode += getNachname().hashCode();
        }
        if (getRezeptGebBefreit() != null) {
            _hashCode += getRezeptGebBefreit().hashCode();
        }
        if (getSVNummer() != null) {
            _hashCode += getSVNummer().hashCode();
        }
        if (getSVTCode() != null) {
            _hashCode += getSVTCode().hashCode();
        }
        if (getVorname() != null) {
            _hashCode += getVorname().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PatientenDaten.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckNachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckNachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckVorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "druckVorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EKVKNummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "EKVKNummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geschlecht");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "geschlecht"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rezeptGebBefreit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "rezeptGebBefreit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVTCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVTCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
