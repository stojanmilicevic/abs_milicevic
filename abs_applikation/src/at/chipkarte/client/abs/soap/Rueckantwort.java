/**
 * Rueckantwort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class Rueckantwort  implements java.io.Serializable {
    private java.lang.String abfrageZeitpunkt;

    private at.chipkarte.client.abs.soap.AnfrageEntscheid[] anfrageEntscheide;

    private java.lang.String anfrageId;

    private java.lang.String antragstyp;

    private java.lang.String beantwortungsZeitpunkt;

    private at.chipkarte.client.abs.soap.BewilligungsAnfrage bewilligungsAnfrage;

    private java.lang.String EKVKPatient;

    private java.lang.String einlangeZeitpunkt;

    private java.lang.String infoText;

    private java.lang.String nachnamePatient;

    private java.lang.String referenzId;

    private java.lang.String SVNRPatient;

    private java.lang.String vornamePatient;

    public Rueckantwort() {
    }

    public Rueckantwort(
           java.lang.String abfrageZeitpunkt,
           at.chipkarte.client.abs.soap.AnfrageEntscheid[] anfrageEntscheide,
           java.lang.String anfrageId,
           java.lang.String antragstyp,
           java.lang.String beantwortungsZeitpunkt,
           at.chipkarte.client.abs.soap.BewilligungsAnfrage bewilligungsAnfrage,
           java.lang.String EKVKPatient,
           java.lang.String einlangeZeitpunkt,
           java.lang.String infoText,
           java.lang.String nachnamePatient,
           java.lang.String referenzId,
           java.lang.String SVNRPatient,
           java.lang.String vornamePatient) {
           this.abfrageZeitpunkt = abfrageZeitpunkt;
           this.anfrageEntscheide = anfrageEntscheide;
           this.anfrageId = anfrageId;
           this.antragstyp = antragstyp;
           this.beantwortungsZeitpunkt = beantwortungsZeitpunkt;
           this.bewilligungsAnfrage = bewilligungsAnfrage;
           this.EKVKPatient = EKVKPatient;
           this.einlangeZeitpunkt = einlangeZeitpunkt;
           this.infoText = infoText;
           this.nachnamePatient = nachnamePatient;
           this.referenzId = referenzId;
           this.SVNRPatient = SVNRPatient;
           this.vornamePatient = vornamePatient;
    }


    /**
     * Gets the abfrageZeitpunkt value for this Rueckantwort.
     * 
     * @return abfrageZeitpunkt
     */
    public java.lang.String getAbfrageZeitpunkt() {
        return abfrageZeitpunkt;
    }


    /**
     * Sets the abfrageZeitpunkt value for this Rueckantwort.
     * 
     * @param abfrageZeitpunkt
     */
    public void setAbfrageZeitpunkt(java.lang.String abfrageZeitpunkt) {
        this.abfrageZeitpunkt = abfrageZeitpunkt;
    }


    /**
     * Gets the anfrageEntscheide value for this Rueckantwort.
     * 
     * @return anfrageEntscheide
     */
    public at.chipkarte.client.abs.soap.AnfrageEntscheid[] getAnfrageEntscheide() {
        return anfrageEntscheide;
    }


    /**
     * Sets the anfrageEntscheide value for this Rueckantwort.
     * 
     * @param anfrageEntscheide
     */
    public void setAnfrageEntscheide(at.chipkarte.client.abs.soap.AnfrageEntscheid[] anfrageEntscheide) {
        this.anfrageEntscheide = anfrageEntscheide;
    }

    public at.chipkarte.client.abs.soap.AnfrageEntscheid getAnfrageEntscheide(int i) {
        return this.anfrageEntscheide[i];
    }

    public void setAnfrageEntscheide(int i, at.chipkarte.client.abs.soap.AnfrageEntscheid _value) {
        this.anfrageEntscheide[i] = _value;
    }


    /**
     * Gets the anfrageId value for this Rueckantwort.
     * 
     * @return anfrageId
     */
    public java.lang.String getAnfrageId() {
        return anfrageId;
    }


    /**
     * Sets the anfrageId value for this Rueckantwort.
     * 
     * @param anfrageId
     */
    public void setAnfrageId(java.lang.String anfrageId) {
        this.anfrageId = anfrageId;
    }


    /**
     * Gets the antragstyp value for this Rueckantwort.
     * 
     * @return antragstyp
     */
    public java.lang.String getAntragstyp() {
        return antragstyp;
    }


    /**
     * Sets the antragstyp value for this Rueckantwort.
     * 
     * @param antragstyp
     */
    public void setAntragstyp(java.lang.String antragstyp) {
        this.antragstyp = antragstyp;
    }


    /**
     * Gets the beantwortungsZeitpunkt value for this Rueckantwort.
     * 
     * @return beantwortungsZeitpunkt
     */
    public java.lang.String getBeantwortungsZeitpunkt() {
        return beantwortungsZeitpunkt;
    }


    /**
     * Sets the beantwortungsZeitpunkt value for this Rueckantwort.
     * 
     * @param beantwortungsZeitpunkt
     */
    public void setBeantwortungsZeitpunkt(java.lang.String beantwortungsZeitpunkt) {
        this.beantwortungsZeitpunkt = beantwortungsZeitpunkt;
    }


    /**
     * Gets the bewilligungsAnfrage value for this Rueckantwort.
     * 
     * @return bewilligungsAnfrage
     */
    public at.chipkarte.client.abs.soap.BewilligungsAnfrage getBewilligungsAnfrage() {
        return bewilligungsAnfrage;
    }


    /**
     * Sets the bewilligungsAnfrage value for this Rueckantwort.
     * 
     * @param bewilligungsAnfrage
     */
    public void setBewilligungsAnfrage(at.chipkarte.client.abs.soap.BewilligungsAnfrage bewilligungsAnfrage) {
        this.bewilligungsAnfrage = bewilligungsAnfrage;
    }


    /**
     * Gets the EKVKPatient value for this Rueckantwort.
     * 
     * @return EKVKPatient
     */
    public java.lang.String getEKVKPatient() {
        return EKVKPatient;
    }


    /**
     * Sets the EKVKPatient value for this Rueckantwort.
     * 
     * @param EKVKPatient
     */
    public void setEKVKPatient(java.lang.String EKVKPatient) {
        this.EKVKPatient = EKVKPatient;
    }


    /**
     * Gets the einlangeZeitpunkt value for this Rueckantwort.
     * 
     * @return einlangeZeitpunkt
     */
    public java.lang.String getEinlangeZeitpunkt() {
        return einlangeZeitpunkt;
    }


    /**
     * Sets the einlangeZeitpunkt value for this Rueckantwort.
     * 
     * @param einlangeZeitpunkt
     */
    public void setEinlangeZeitpunkt(java.lang.String einlangeZeitpunkt) {
        this.einlangeZeitpunkt = einlangeZeitpunkt;
    }


    /**
     * Gets the infoText value for this Rueckantwort.
     * 
     * @return infoText
     */
    public java.lang.String getInfoText() {
        return infoText;
    }


    /**
     * Sets the infoText value for this Rueckantwort.
     * 
     * @param infoText
     */
    public void setInfoText(java.lang.String infoText) {
        this.infoText = infoText;
    }


    /**
     * Gets the nachnamePatient value for this Rueckantwort.
     * 
     * @return nachnamePatient
     */
    public java.lang.String getNachnamePatient() {
        return nachnamePatient;
    }


    /**
     * Sets the nachnamePatient value for this Rueckantwort.
     * 
     * @param nachnamePatient
     */
    public void setNachnamePatient(java.lang.String nachnamePatient) {
        this.nachnamePatient = nachnamePatient;
    }


    /**
     * Gets the referenzId value for this Rueckantwort.
     * 
     * @return referenzId
     */
    public java.lang.String getReferenzId() {
        return referenzId;
    }


    /**
     * Sets the referenzId value for this Rueckantwort.
     * 
     * @param referenzId
     */
    public void setReferenzId(java.lang.String referenzId) {
        this.referenzId = referenzId;
    }


    /**
     * Gets the SVNRPatient value for this Rueckantwort.
     * 
     * @return SVNRPatient
     */
    public java.lang.String getSVNRPatient() {
        return SVNRPatient;
    }


    /**
     * Sets the SVNRPatient value for this Rueckantwort.
     * 
     * @param SVNRPatient
     */
    public void setSVNRPatient(java.lang.String SVNRPatient) {
        this.SVNRPatient = SVNRPatient;
    }


    /**
     * Gets the vornamePatient value for this Rueckantwort.
     * 
     * @return vornamePatient
     */
    public java.lang.String getVornamePatient() {
        return vornamePatient;
    }


    /**
     * Sets the vornamePatient value for this Rueckantwort.
     * 
     * @param vornamePatient
     */
    public void setVornamePatient(java.lang.String vornamePatient) {
        this.vornamePatient = vornamePatient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Rueckantwort)) return false;
        Rueckantwort other = (Rueckantwort) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abfrageZeitpunkt==null && other.getAbfrageZeitpunkt()==null) || 
             (this.abfrageZeitpunkt!=null &&
              this.abfrageZeitpunkt.equals(other.getAbfrageZeitpunkt()))) &&
            ((this.anfrageEntscheide==null && other.getAnfrageEntscheide()==null) || 
             (this.anfrageEntscheide!=null &&
              java.util.Arrays.equals(this.anfrageEntscheide, other.getAnfrageEntscheide()))) &&
            ((this.anfrageId==null && other.getAnfrageId()==null) || 
             (this.anfrageId!=null &&
              this.anfrageId.equals(other.getAnfrageId()))) &&
            ((this.antragstyp==null && other.getAntragstyp()==null) || 
             (this.antragstyp!=null &&
              this.antragstyp.equals(other.getAntragstyp()))) &&
            ((this.beantwortungsZeitpunkt==null && other.getBeantwortungsZeitpunkt()==null) || 
             (this.beantwortungsZeitpunkt!=null &&
              this.beantwortungsZeitpunkt.equals(other.getBeantwortungsZeitpunkt()))) &&
            ((this.bewilligungsAnfrage==null && other.getBewilligungsAnfrage()==null) || 
             (this.bewilligungsAnfrage!=null &&
              this.bewilligungsAnfrage.equals(other.getBewilligungsAnfrage()))) &&
            ((this.EKVKPatient==null && other.getEKVKPatient()==null) || 
             (this.EKVKPatient!=null &&
              this.EKVKPatient.equals(other.getEKVKPatient()))) &&
            ((this.einlangeZeitpunkt==null && other.getEinlangeZeitpunkt()==null) || 
             (this.einlangeZeitpunkt!=null &&
              this.einlangeZeitpunkt.equals(other.getEinlangeZeitpunkt()))) &&
            ((this.infoText==null && other.getInfoText()==null) || 
             (this.infoText!=null &&
              this.infoText.equals(other.getInfoText()))) &&
            ((this.nachnamePatient==null && other.getNachnamePatient()==null) || 
             (this.nachnamePatient!=null &&
              this.nachnamePatient.equals(other.getNachnamePatient()))) &&
            ((this.referenzId==null && other.getReferenzId()==null) || 
             (this.referenzId!=null &&
              this.referenzId.equals(other.getReferenzId()))) &&
            ((this.SVNRPatient==null && other.getSVNRPatient()==null) || 
             (this.SVNRPatient!=null &&
              this.SVNRPatient.equals(other.getSVNRPatient()))) &&
            ((this.vornamePatient==null && other.getVornamePatient()==null) || 
             (this.vornamePatient!=null &&
              this.vornamePatient.equals(other.getVornamePatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbfrageZeitpunkt() != null) {
            _hashCode += getAbfrageZeitpunkt().hashCode();
        }
        if (getAnfrageEntscheide() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAnfrageEntscheide());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAnfrageEntscheide(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAnfrageId() != null) {
            _hashCode += getAnfrageId().hashCode();
        }
        if (getAntragstyp() != null) {
            _hashCode += getAntragstyp().hashCode();
        }
        if (getBeantwortungsZeitpunkt() != null) {
            _hashCode += getBeantwortungsZeitpunkt().hashCode();
        }
        if (getBewilligungsAnfrage() != null) {
            _hashCode += getBewilligungsAnfrage().hashCode();
        }
        if (getEKVKPatient() != null) {
            _hashCode += getEKVKPatient().hashCode();
        }
        if (getEinlangeZeitpunkt() != null) {
            _hashCode += getEinlangeZeitpunkt().hashCode();
        }
        if (getInfoText() != null) {
            _hashCode += getInfoText().hashCode();
        }
        if (getNachnamePatient() != null) {
            _hashCode += getNachnamePatient().hashCode();
        }
        if (getReferenzId() != null) {
            _hashCode += getReferenzId().hashCode();
        }
        if (getSVNRPatient() != null) {
            _hashCode += getSVNRPatient().hashCode();
        }
        if (getVornamePatient() != null) {
            _hashCode += getVornamePatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Rueckantwort.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "rueckantwort"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abfrageZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "abfrageZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anfrageEntscheide");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageEntscheide"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageEntscheid"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anfrageId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antragstyp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "antragstyp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantwortungsZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantwortungsZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bewilligungsAnfrage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "bewilligungsAnfrage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EKVKPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "EKVKPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("einlangeZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "einlangeZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "infoText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenzId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "referenzId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNRPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNRPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
