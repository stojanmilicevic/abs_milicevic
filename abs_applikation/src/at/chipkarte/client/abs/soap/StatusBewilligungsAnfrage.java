/**
 * StatusBewilligungsAnfrage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class StatusBewilligungsAnfrage  implements java.io.Serializable {
    private java.lang.String abfrageZeitpunkt;

    private java.lang.String anfrageId;

    private java.lang.String beantwortungsZeitpunkt;

    private java.lang.String EKVKNummer;

    private java.lang.String einlangeZeitpunkt;

    private java.lang.String nachnamePatient;

    private java.lang.String SVNRPatient;

    private java.lang.String vornamePatient;

    public StatusBewilligungsAnfrage() {
    }

    public StatusBewilligungsAnfrage(
           java.lang.String abfrageZeitpunkt,
           java.lang.String anfrageId,
           java.lang.String beantwortungsZeitpunkt,
           java.lang.String EKVKNummer,
           java.lang.String einlangeZeitpunkt,
           java.lang.String nachnamePatient,
           java.lang.String SVNRPatient,
           java.lang.String vornamePatient) {
           this.abfrageZeitpunkt = abfrageZeitpunkt;
           this.anfrageId = anfrageId;
           this.beantwortungsZeitpunkt = beantwortungsZeitpunkt;
           this.EKVKNummer = EKVKNummer;
           this.einlangeZeitpunkt = einlangeZeitpunkt;
           this.nachnamePatient = nachnamePatient;
           this.SVNRPatient = SVNRPatient;
           this.vornamePatient = vornamePatient;
    }


    /**
     * Gets the abfrageZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @return abfrageZeitpunkt
     */
    public java.lang.String getAbfrageZeitpunkt() {
        return abfrageZeitpunkt;
    }


    /**
     * Sets the abfrageZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @param abfrageZeitpunkt
     */
    public void setAbfrageZeitpunkt(java.lang.String abfrageZeitpunkt) {
        this.abfrageZeitpunkt = abfrageZeitpunkt;
    }


    /**
     * Gets the anfrageId value for this StatusBewilligungsAnfrage.
     * 
     * @return anfrageId
     */
    public java.lang.String getAnfrageId() {
        return anfrageId;
    }


    /**
     * Sets the anfrageId value for this StatusBewilligungsAnfrage.
     * 
     * @param anfrageId
     */
    public void setAnfrageId(java.lang.String anfrageId) {
        this.anfrageId = anfrageId;
    }


    /**
     * Gets the beantwortungsZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @return beantwortungsZeitpunkt
     */
    public java.lang.String getBeantwortungsZeitpunkt() {
        return beantwortungsZeitpunkt;
    }


    /**
     * Sets the beantwortungsZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @param beantwortungsZeitpunkt
     */
    public void setBeantwortungsZeitpunkt(java.lang.String beantwortungsZeitpunkt) {
        this.beantwortungsZeitpunkt = beantwortungsZeitpunkt;
    }


    /**
     * Gets the EKVKNummer value for this StatusBewilligungsAnfrage.
     * 
     * @return EKVKNummer
     */
    public java.lang.String getEKVKNummer() {
        return EKVKNummer;
    }


    /**
     * Sets the EKVKNummer value for this StatusBewilligungsAnfrage.
     * 
     * @param EKVKNummer
     */
    public void setEKVKNummer(java.lang.String EKVKNummer) {
        this.EKVKNummer = EKVKNummer;
    }


    /**
     * Gets the einlangeZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @return einlangeZeitpunkt
     */
    public java.lang.String getEinlangeZeitpunkt() {
        return einlangeZeitpunkt;
    }


    /**
     * Sets the einlangeZeitpunkt value for this StatusBewilligungsAnfrage.
     * 
     * @param einlangeZeitpunkt
     */
    public void setEinlangeZeitpunkt(java.lang.String einlangeZeitpunkt) {
        this.einlangeZeitpunkt = einlangeZeitpunkt;
    }


    /**
     * Gets the nachnamePatient value for this StatusBewilligungsAnfrage.
     * 
     * @return nachnamePatient
     */
    public java.lang.String getNachnamePatient() {
        return nachnamePatient;
    }


    /**
     * Sets the nachnamePatient value for this StatusBewilligungsAnfrage.
     * 
     * @param nachnamePatient
     */
    public void setNachnamePatient(java.lang.String nachnamePatient) {
        this.nachnamePatient = nachnamePatient;
    }


    /**
     * Gets the SVNRPatient value for this StatusBewilligungsAnfrage.
     * 
     * @return SVNRPatient
     */
    public java.lang.String getSVNRPatient() {
        return SVNRPatient;
    }


    /**
     * Sets the SVNRPatient value for this StatusBewilligungsAnfrage.
     * 
     * @param SVNRPatient
     */
    public void setSVNRPatient(java.lang.String SVNRPatient) {
        this.SVNRPatient = SVNRPatient;
    }


    /**
     * Gets the vornamePatient value for this StatusBewilligungsAnfrage.
     * 
     * @return vornamePatient
     */
    public java.lang.String getVornamePatient() {
        return vornamePatient;
    }


    /**
     * Sets the vornamePatient value for this StatusBewilligungsAnfrage.
     * 
     * @param vornamePatient
     */
    public void setVornamePatient(java.lang.String vornamePatient) {
        this.vornamePatient = vornamePatient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatusBewilligungsAnfrage)) return false;
        StatusBewilligungsAnfrage other = (StatusBewilligungsAnfrage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abfrageZeitpunkt==null && other.getAbfrageZeitpunkt()==null) || 
             (this.abfrageZeitpunkt!=null &&
              this.abfrageZeitpunkt.equals(other.getAbfrageZeitpunkt()))) &&
            ((this.anfrageId==null && other.getAnfrageId()==null) || 
             (this.anfrageId!=null &&
              this.anfrageId.equals(other.getAnfrageId()))) &&
            ((this.beantwortungsZeitpunkt==null && other.getBeantwortungsZeitpunkt()==null) || 
             (this.beantwortungsZeitpunkt!=null &&
              this.beantwortungsZeitpunkt.equals(other.getBeantwortungsZeitpunkt()))) &&
            ((this.EKVKNummer==null && other.getEKVKNummer()==null) || 
             (this.EKVKNummer!=null &&
              this.EKVKNummer.equals(other.getEKVKNummer()))) &&
            ((this.einlangeZeitpunkt==null && other.getEinlangeZeitpunkt()==null) || 
             (this.einlangeZeitpunkt!=null &&
              this.einlangeZeitpunkt.equals(other.getEinlangeZeitpunkt()))) &&
            ((this.nachnamePatient==null && other.getNachnamePatient()==null) || 
             (this.nachnamePatient!=null &&
              this.nachnamePatient.equals(other.getNachnamePatient()))) &&
            ((this.SVNRPatient==null && other.getSVNRPatient()==null) || 
             (this.SVNRPatient!=null &&
              this.SVNRPatient.equals(other.getSVNRPatient()))) &&
            ((this.vornamePatient==null && other.getVornamePatient()==null) || 
             (this.vornamePatient!=null &&
              this.vornamePatient.equals(other.getVornamePatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbfrageZeitpunkt() != null) {
            _hashCode += getAbfrageZeitpunkt().hashCode();
        }
        if (getAnfrageId() != null) {
            _hashCode += getAnfrageId().hashCode();
        }
        if (getBeantwortungsZeitpunkt() != null) {
            _hashCode += getBeantwortungsZeitpunkt().hashCode();
        }
        if (getEKVKNummer() != null) {
            _hashCode += getEKVKNummer().hashCode();
        }
        if (getEinlangeZeitpunkt() != null) {
            _hashCode += getEinlangeZeitpunkt().hashCode();
        }
        if (getNachnamePatient() != null) {
            _hashCode += getNachnamePatient().hashCode();
        }
        if (getSVNRPatient() != null) {
            _hashCode += getSVNRPatient().hashCode();
        }
        if (getVornamePatient() != null) {
            _hashCode += getVornamePatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatusBewilligungsAnfrage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "statusBewilligungsAnfrage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abfrageZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "abfrageZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anfrageId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "anfrageId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beantwortungsZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "beantwortungsZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EKVKNummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "EKVKNummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("einlangeZeitpunkt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "einlangeZeitpunkt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachnamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "nachnamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SVNRPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "SVNRPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vornamePatient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "vornamePatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
