/**
 * Verordnung.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap;

public class Verordnung  implements java.io.Serializable {
    private java.lang.String begruendung;

    private java.lang.String diagnose;

    private java.lang.String dosierung;

    private java.lang.String langzeitverordnung;

    private java.lang.String magistraleZubereitung;

    private at.chipkarte.client.abs.soap.Medikament medikament;

    private java.lang.Integer packungsanzahl;

    public Verordnung() {
    }

    public Verordnung(
           java.lang.String begruendung,
           java.lang.String diagnose,
           java.lang.String dosierung,
           java.lang.String langzeitverordnung,
           java.lang.String magistraleZubereitung,
           at.chipkarte.client.abs.soap.Medikament medikament,
           java.lang.Integer packungsanzahl) {
           this.begruendung = begruendung;
           this.diagnose = diagnose;
           this.dosierung = dosierung;
           this.langzeitverordnung = langzeitverordnung;
           this.magistraleZubereitung = magistraleZubereitung;
           this.medikament = medikament;
           this.packungsanzahl = packungsanzahl;
    }


    /**
     * Gets the begruendung value for this Verordnung.
     * 
     * @return begruendung
     */
    public java.lang.String getBegruendung() {
        return begruendung;
    }


    /**
     * Sets the begruendung value for this Verordnung.
     * 
     * @param begruendung
     */
    public void setBegruendung(java.lang.String begruendung) {
        this.begruendung = begruendung;
    }


    /**
     * Gets the diagnose value for this Verordnung.
     * 
     * @return diagnose
     */
    public java.lang.String getDiagnose() {
        return diagnose;
    }


    /**
     * Sets the diagnose value for this Verordnung.
     * 
     * @param diagnose
     */
    public void setDiagnose(java.lang.String diagnose) {
        this.diagnose = diagnose;
    }


    /**
     * Gets the dosierung value for this Verordnung.
     * 
     * @return dosierung
     */
    public java.lang.String getDosierung() {
        return dosierung;
    }


    /**
     * Sets the dosierung value for this Verordnung.
     * 
     * @param dosierung
     */
    public void setDosierung(java.lang.String dosierung) {
        this.dosierung = dosierung;
    }


    /**
     * Gets the langzeitverordnung value for this Verordnung.
     * 
     * @return langzeitverordnung
     */
    public java.lang.String getLangzeitverordnung() {
        return langzeitverordnung;
    }


    /**
     * Sets the langzeitverordnung value for this Verordnung.
     * 
     * @param langzeitverordnung
     */
    public void setLangzeitverordnung(java.lang.String langzeitverordnung) {
        this.langzeitverordnung = langzeitverordnung;
    }


    /**
     * Gets the magistraleZubereitung value for this Verordnung.
     * 
     * @return magistraleZubereitung
     */
    public java.lang.String getMagistraleZubereitung() {
        return magistraleZubereitung;
    }


    /**
     * Sets the magistraleZubereitung value for this Verordnung.
     * 
     * @param magistraleZubereitung
     */
    public void setMagistraleZubereitung(java.lang.String magistraleZubereitung) {
        this.magistraleZubereitung = magistraleZubereitung;
    }


    /**
     * Gets the medikament value for this Verordnung.
     * 
     * @return medikament
     */
    public at.chipkarte.client.abs.soap.Medikament getMedikament() {
        return medikament;
    }


    /**
     * Sets the medikament value for this Verordnung.
     * 
     * @param medikament
     */
    public void setMedikament(at.chipkarte.client.abs.soap.Medikament medikament) {
        this.medikament = medikament;
    }


    /**
     * Gets the packungsanzahl value for this Verordnung.
     * 
     * @return packungsanzahl
     */
    public java.lang.Integer getPackungsanzahl() {
        return packungsanzahl;
    }


    /**
     * Sets the packungsanzahl value for this Verordnung.
     * 
     * @param packungsanzahl
     */
    public void setPackungsanzahl(java.lang.Integer packungsanzahl) {
        this.packungsanzahl = packungsanzahl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Verordnung)) return false;
        Verordnung other = (Verordnung) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.begruendung==null && other.getBegruendung()==null) || 
             (this.begruendung!=null &&
              this.begruendung.equals(other.getBegruendung()))) &&
            ((this.diagnose==null && other.getDiagnose()==null) || 
             (this.diagnose!=null &&
              this.diagnose.equals(other.getDiagnose()))) &&
            ((this.dosierung==null && other.getDosierung()==null) || 
             (this.dosierung!=null &&
              this.dosierung.equals(other.getDosierung()))) &&
            ((this.langzeitverordnung==null && other.getLangzeitverordnung()==null) || 
             (this.langzeitverordnung!=null &&
              this.langzeitverordnung.equals(other.getLangzeitverordnung()))) &&
            ((this.magistraleZubereitung==null && other.getMagistraleZubereitung()==null) || 
             (this.magistraleZubereitung!=null &&
              this.magistraleZubereitung.equals(other.getMagistraleZubereitung()))) &&
            ((this.medikament==null && other.getMedikament()==null) || 
             (this.medikament!=null &&
              this.medikament.equals(other.getMedikament()))) &&
            ((this.packungsanzahl==null && other.getPackungsanzahl()==null) || 
             (this.packungsanzahl!=null &&
              this.packungsanzahl.equals(other.getPackungsanzahl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBegruendung() != null) {
            _hashCode += getBegruendung().hashCode();
        }
        if (getDiagnose() != null) {
            _hashCode += getDiagnose().hashCode();
        }
        if (getDosierung() != null) {
            _hashCode += getDosierung().hashCode();
        }
        if (getLangzeitverordnung() != null) {
            _hashCode += getLangzeitverordnung().hashCode();
        }
        if (getMagistraleZubereitung() != null) {
            _hashCode += getMagistraleZubereitung().hashCode();
        }
        if (getMedikament() != null) {
            _hashCode += getMedikament().hashCode();
        }
        if (getPackungsanzahl() != null) {
            _hashCode += getPackungsanzahl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Verordnung.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "verordnung"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("begruendung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "begruendung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diagnose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "diagnose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dosierung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "dosierung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("langzeitverordnung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "langzeitverordnung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magistraleZubereitung");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "magistraleZubereitung"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medikament");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikament"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "medikament"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packungsanzahl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "packungsanzahl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
