/**
 * AbsExceptionContent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.abs.soap.exceptions;

public class AbsExceptionContent  extends at.chipkarte.client.base.soap.exceptions.BaseExceptionContent  implements java.io.Serializable {
    private java.lang.String[] KVTCodes;

    private at.chipkarte.client.abs.soap.PatientenDaten patientenDaten;

    public AbsExceptionContent() {
    }

    public AbsExceptionContent(
           java.lang.String code,
           java.lang.Integer errorCode,
           java.lang.String message1,
           java.lang.String[] KVTCodes,
           at.chipkarte.client.abs.soap.PatientenDaten patientenDaten) {
        super(
            code,
            errorCode,
            message1);
        this.KVTCodes = KVTCodes;
        this.patientenDaten = patientenDaten;
    }


    /**
     * Gets the KVTCodes value for this AbsExceptionContent.
     * 
     * @return KVTCodes
     */
    public java.lang.String[] getKVTCodes() {
        return KVTCodes;
    }


    /**
     * Sets the KVTCodes value for this AbsExceptionContent.
     * 
     * @param KVTCodes
     */
    public void setKVTCodes(java.lang.String[] KVTCodes) {
        this.KVTCodes = KVTCodes;
    }

    public java.lang.String getKVTCodes(int i) {
        return this.KVTCodes[i];
    }

    public void setKVTCodes(int i, java.lang.String _value) {
        this.KVTCodes[i] = _value;
    }


    /**
     * Gets the patientenDaten value for this AbsExceptionContent.
     * 
     * @return patientenDaten
     */
    public at.chipkarte.client.abs.soap.PatientenDaten getPatientenDaten() {
        return patientenDaten;
    }


    /**
     * Sets the patientenDaten value for this AbsExceptionContent.
     * 
     * @param patientenDaten
     */
    public void setPatientenDaten(at.chipkarte.client.abs.soap.PatientenDaten patientenDaten) {
        this.patientenDaten = patientenDaten;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AbsExceptionContent)) return false;
        AbsExceptionContent other = (AbsExceptionContent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.KVTCodes==null && other.getKVTCodes()==null) || 
             (this.KVTCodes!=null &&
              java.util.Arrays.equals(this.KVTCodes, other.getKVTCodes()))) &&
            ((this.patientenDaten==null && other.getPatientenDaten()==null) || 
             (this.patientenDaten!=null &&
              this.patientenDaten.equals(other.getPatientenDaten())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getKVTCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getKVTCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getKVTCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPatientenDaten() != null) {
            _hashCode += getPatientenDaten().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AbsExceptionContent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "absExceptionContent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("KVTCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "KVTCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientenDaten");
        elemField.setXmlName(new javax.xml.namespace.QName("http://exceptions.soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.abs.client.chipkarte.at", "patientenDaten"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
