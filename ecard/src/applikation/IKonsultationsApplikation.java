package applikation;

import java.util.Date;
import java.util.List;

import at.chipkarte.client.base.soap.CardReader;
import at.chipkarte.client.kse.soap.Konsultationsdaten;

public interface IKonsultationsApplikation  
{
	/**
	 * liefert in einem Array alle verf�gbaren Reader
	 * @return Reader
	 * @throws Exception
	 */
	public CardReader[] alleReader() throws Exception;
	
	
	/**
	 * schlie�t den Dialog
	 */
	public void abmelden();
	
	
	/**
	 * Anmeldung am System, es muss der ausgew�hlte 
	 * o-Card-Reader und die PIN �bergeben werden
	 * 
	 * @param oCardReader  o-Card Reader
	 * @param pin
	 * @return  true, falls Anmeldung gut geht, sonst false
	 * @throws Exception  Verbindungsprobleme
	 */
	public boolean anmelden(CardReader oCardReader, String pin) throws Exception;
	
	
	/**
	 * eruiert alle m�glichen Behandlungsf�lle
	 * @return Liste von Behandlungsfall-Codes
	 * @throws Exception
	 */
	
}