/**
 * SvPersonV2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.base.soap;

public class SvPersonV2  implements java.io.Serializable {
    private java.lang.String druckNachname;

    private java.lang.String druckTitelHinten;

    private java.lang.String druckTitelVorne;

    private java.lang.String druckVorname;

    private java.lang.String geburtsdatum;

    private java.lang.String geschlecht;

    private java.lang.String nachname;

    private java.lang.String svNummer;

    private java.lang.String titelHinten;

    private java.lang.String titelVorne;

    private java.lang.String vorname;

    public SvPersonV2() {
    }

    public SvPersonV2(
           java.lang.String druckNachname,
           java.lang.String druckTitelHinten,
           java.lang.String druckTitelVorne,
           java.lang.String druckVorname,
           java.lang.String geburtsdatum,
           java.lang.String geschlecht,
           java.lang.String nachname,
           java.lang.String svNummer,
           java.lang.String titelHinten,
           java.lang.String titelVorne,
           java.lang.String vorname) {
           this.druckNachname = druckNachname;
           this.druckTitelHinten = druckTitelHinten;
           this.druckTitelVorne = druckTitelVorne;
           this.druckVorname = druckVorname;
           this.geburtsdatum = geburtsdatum;
           this.geschlecht = geschlecht;
           this.nachname = nachname;
           this.svNummer = svNummer;
           this.titelHinten = titelHinten;
           this.titelVorne = titelVorne;
           this.vorname = vorname;
    }


    /**
     * Gets the druckNachname value for this SvPersonV2.
     * 
     * @return druckNachname
     */
    public java.lang.String getDruckNachname() {
        return druckNachname;
    }


    /**
     * Sets the druckNachname value for this SvPersonV2.
     * 
     * @param druckNachname
     */
    public void setDruckNachname(java.lang.String druckNachname) {
        this.druckNachname = druckNachname;
    }


    /**
     * Gets the druckTitelHinten value for this SvPersonV2.
     * 
     * @return druckTitelHinten
     */
    public java.lang.String getDruckTitelHinten() {
        return druckTitelHinten;
    }


    /**
     * Sets the druckTitelHinten value for this SvPersonV2.
     * 
     * @param druckTitelHinten
     */
    public void setDruckTitelHinten(java.lang.String druckTitelHinten) {
        this.druckTitelHinten = druckTitelHinten;
    }


    /**
     * Gets the druckTitelVorne value for this SvPersonV2.
     * 
     * @return druckTitelVorne
     */
    public java.lang.String getDruckTitelVorne() {
        return druckTitelVorne;
    }


    /**
     * Sets the druckTitelVorne value for this SvPersonV2.
     * 
     * @param druckTitelVorne
     */
    public void setDruckTitelVorne(java.lang.String druckTitelVorne) {
        this.druckTitelVorne = druckTitelVorne;
    }


    /**
     * Gets the druckVorname value for this SvPersonV2.
     * 
     * @return druckVorname
     */
    public java.lang.String getDruckVorname() {
        return druckVorname;
    }


    /**
     * Sets the druckVorname value for this SvPersonV2.
     * 
     * @param druckVorname
     */
    public void setDruckVorname(java.lang.String druckVorname) {
        this.druckVorname = druckVorname;
    }


    /**
     * Gets the geburtsdatum value for this SvPersonV2.
     * 
     * @return geburtsdatum
     */
    public java.lang.String getGeburtsdatum() {
        return geburtsdatum;
    }


    /**
     * Sets the geburtsdatum value for this SvPersonV2.
     * 
     * @param geburtsdatum
     */
    public void setGeburtsdatum(java.lang.String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }


    /**
     * Gets the geschlecht value for this SvPersonV2.
     * 
     * @return geschlecht
     */
    public java.lang.String getGeschlecht() {
        return geschlecht;
    }


    /**
     * Sets the geschlecht value for this SvPersonV2.
     * 
     * @param geschlecht
     */
    public void setGeschlecht(java.lang.String geschlecht) {
        this.geschlecht = geschlecht;
    }


    /**
     * Gets the nachname value for this SvPersonV2.
     * 
     * @return nachname
     */
    public java.lang.String getNachname() {
        return nachname;
    }


    /**
     * Sets the nachname value for this SvPersonV2.
     * 
     * @param nachname
     */
    public void setNachname(java.lang.String nachname) {
        this.nachname = nachname;
    }


    /**
     * Gets the svNummer value for this SvPersonV2.
     * 
     * @return svNummer
     */
    public java.lang.String getSvNummer() {
        return svNummer;
    }


    /**
     * Sets the svNummer value for this SvPersonV2.
     * 
     * @param svNummer
     */
    public void setSvNummer(java.lang.String svNummer) {
        this.svNummer = svNummer;
    }


    /**
     * Gets the titelHinten value for this SvPersonV2.
     * 
     * @return titelHinten
     */
    public java.lang.String getTitelHinten() {
        return titelHinten;
    }


    /**
     * Sets the titelHinten value for this SvPersonV2.
     * 
     * @param titelHinten
     */
    public void setTitelHinten(java.lang.String titelHinten) {
        this.titelHinten = titelHinten;
    }


    /**
     * Gets the titelVorne value for this SvPersonV2.
     * 
     * @return titelVorne
     */
    public java.lang.String getTitelVorne() {
        return titelVorne;
    }


    /**
     * Sets the titelVorne value for this SvPersonV2.
     * 
     * @param titelVorne
     */
    public void setTitelVorne(java.lang.String titelVorne) {
        this.titelVorne = titelVorne;
    }


    /**
     * Gets the vorname value for this SvPersonV2.
     * 
     * @return vorname
     */
    public java.lang.String getVorname() {
        return vorname;
    }


    /**
     * Sets the vorname value for this SvPersonV2.
     * 
     * @param vorname
     */
    public void setVorname(java.lang.String vorname) {
        this.vorname = vorname;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvPersonV2)) return false;
        SvPersonV2 other = (SvPersonV2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.druckNachname==null && other.getDruckNachname()==null) || 
             (this.druckNachname!=null &&
              this.druckNachname.equals(other.getDruckNachname()))) &&
            ((this.druckTitelHinten==null && other.getDruckTitelHinten()==null) || 
             (this.druckTitelHinten!=null &&
              this.druckTitelHinten.equals(other.getDruckTitelHinten()))) &&
            ((this.druckTitelVorne==null && other.getDruckTitelVorne()==null) || 
             (this.druckTitelVorne!=null &&
              this.druckTitelVorne.equals(other.getDruckTitelVorne()))) &&
            ((this.druckVorname==null && other.getDruckVorname()==null) || 
             (this.druckVorname!=null &&
              this.druckVorname.equals(other.getDruckVorname()))) &&
            ((this.geburtsdatum==null && other.getGeburtsdatum()==null) || 
             (this.geburtsdatum!=null &&
              this.geburtsdatum.equals(other.getGeburtsdatum()))) &&
            ((this.geschlecht==null && other.getGeschlecht()==null) || 
             (this.geschlecht!=null &&
              this.geschlecht.equals(other.getGeschlecht()))) &&
            ((this.nachname==null && other.getNachname()==null) || 
             (this.nachname!=null &&
              this.nachname.equals(other.getNachname()))) &&
            ((this.svNummer==null && other.getSvNummer()==null) || 
             (this.svNummer!=null &&
              this.svNummer.equals(other.getSvNummer()))) &&
            ((this.titelHinten==null && other.getTitelHinten()==null) || 
             (this.titelHinten!=null &&
              this.titelHinten.equals(other.getTitelHinten()))) &&
            ((this.titelVorne==null && other.getTitelVorne()==null) || 
             (this.titelVorne!=null &&
              this.titelVorne.equals(other.getTitelVorne()))) &&
            ((this.vorname==null && other.getVorname()==null) || 
             (this.vorname!=null &&
              this.vorname.equals(other.getVorname())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDruckNachname() != null) {
            _hashCode += getDruckNachname().hashCode();
        }
        if (getDruckTitelHinten() != null) {
            _hashCode += getDruckTitelHinten().hashCode();
        }
        if (getDruckTitelVorne() != null) {
            _hashCode += getDruckTitelVorne().hashCode();
        }
        if (getDruckVorname() != null) {
            _hashCode += getDruckVorname().hashCode();
        }
        if (getGeburtsdatum() != null) {
            _hashCode += getGeburtsdatum().hashCode();
        }
        if (getGeschlecht() != null) {
            _hashCode += getGeschlecht().hashCode();
        }
        if (getNachname() != null) {
            _hashCode += getNachname().hashCode();
        }
        if (getSvNummer() != null) {
            _hashCode += getSvNummer().hashCode();
        }
        if (getTitelHinten() != null) {
            _hashCode += getTitelHinten().hashCode();
        }
        if (getTitelVorne() != null) {
            _hashCode += getTitelVorne().hashCode();
        }
        if (getVorname() != null) {
            _hashCode += getVorname().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvPersonV2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "svPersonV2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckNachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "druckNachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckTitelHinten");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "druckTitelHinten"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckTitelVorne");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "druckTitelVorne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("druckVorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "druckVorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geburtsdatum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "geburtsdatum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geschlecht");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "geschlecht"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "nachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("svNummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "svNummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titelHinten");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "titelHinten"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titelVorne");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "titelVorne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "vorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
