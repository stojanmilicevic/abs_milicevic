/**
 * BehandlungsfallZusatzProperty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.kse.soap;

public class BehandlungsfallZusatzProperty  implements java.io.Serializable {
    private at.chipkarte.client.base.soap.BaseProperty baseProperty;

    private java.lang.String zusatzRecht;

    public BehandlungsfallZusatzProperty() {
    }

    public BehandlungsfallZusatzProperty(
           at.chipkarte.client.base.soap.BaseProperty baseProperty,
           java.lang.String zusatzRecht) {
           this.baseProperty = baseProperty;
           this.zusatzRecht = zusatzRecht;
    }


    /**
     * Gets the baseProperty value for this BehandlungsfallZusatzProperty.
     * 
     * @return baseProperty
     */
    public at.chipkarte.client.base.soap.BaseProperty getBaseProperty() {
        return baseProperty;
    }


    /**
     * Sets the baseProperty value for this BehandlungsfallZusatzProperty.
     * 
     * @param baseProperty
     */
    public void setBaseProperty(at.chipkarte.client.base.soap.BaseProperty baseProperty) {
        this.baseProperty = baseProperty;
    }


    /**
     * Gets the zusatzRecht value for this BehandlungsfallZusatzProperty.
     * 
     * @return zusatzRecht
     */
    public java.lang.String getZusatzRecht() {
        return zusatzRecht;
    }


    /**
     * Sets the zusatzRecht value for this BehandlungsfallZusatzProperty.
     * 
     * @param zusatzRecht
     */
    public void setZusatzRecht(java.lang.String zusatzRecht) {
        this.zusatzRecht = zusatzRecht;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BehandlungsfallZusatzProperty)) return false;
        BehandlungsfallZusatzProperty other = (BehandlungsfallZusatzProperty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baseProperty==null && other.getBaseProperty()==null) || 
             (this.baseProperty!=null &&
              this.baseProperty.equals(other.getBaseProperty()))) &&
            ((this.zusatzRecht==null && other.getZusatzRecht()==null) || 
             (this.zusatzRecht!=null &&
              this.zusatzRecht.equals(other.getZusatzRecht())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaseProperty() != null) {
            _hashCode += getBaseProperty().hashCode();
        }
        if (getZusatzRecht() != null) {
            _hashCode += getZusatzRecht().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BehandlungsfallZusatzProperty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallZusatzProperty"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseProperty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "baseProperty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "baseProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zusatzRecht");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "zusatzRecht"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
