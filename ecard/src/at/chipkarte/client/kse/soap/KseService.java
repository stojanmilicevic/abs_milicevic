/**
 * KseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.kse.soap;

public interface KseService extends javax.xml.rpc.Service {
    public java.lang.String getkse_17Address();

    public at.chipkarte.client.kse.soap.IKseService getkse_17() throws javax.xml.rpc.ServiceException;

    public at.chipkarte.client.kse.soap.IKseService getkse_17(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
