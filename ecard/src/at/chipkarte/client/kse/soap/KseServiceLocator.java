/**
 * KseServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.chipkarte.client.kse.soap;

public class KseServiceLocator extends org.apache.axis.client.Service implements at.chipkarte.client.kse.soap.KseService {

    public KseServiceLocator() {
    }


    public KseServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public KseServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for kse_17
    private java.lang.String kse_17_address = "https://10.196.2.18/kse/17";

    public java.lang.String getkse_17Address() {
        return kse_17_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String kse_17WSDDServiceName = "kse_17";

    public java.lang.String getkse_17WSDDServiceName() {
        return kse_17WSDDServiceName;
    }

    public void setkse_17WSDDServiceName(java.lang.String name) {
        kse_17WSDDServiceName = name;
    }

    public at.chipkarte.client.kse.soap.IKseService getkse_17() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(kse_17_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getkse_17(endpoint);
    }

    public at.chipkarte.client.kse.soap.IKseService getkse_17(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            at.chipkarte.client.kse.soap.Kse_17BindingStub _stub = new at.chipkarte.client.kse.soap.Kse_17BindingStub(portAddress, this);
            _stub.setPortName(getkse_17WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setkse_17EndpointAddress(java.lang.String address) {
        kse_17_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (at.chipkarte.client.kse.soap.IKseService.class.isAssignableFrom(serviceEndpointInterface)) {
                at.chipkarte.client.kse.soap.Kse_17BindingStub _stub = new at.chipkarte.client.kse.soap.Kse_17BindingStub(new java.net.URL(kse_17_address), this);
                _stub.setPortName(getkse_17WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("kse_17".equals(inputPortName)) {
            return getkse_17();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "KseService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "kse_17"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("kse_17".equals(portName)) {
            setkse_17EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
