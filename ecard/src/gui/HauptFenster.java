package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import applikation.IKonsultationsApplikation;
import at.chipkarte.client.base.soap.CardReader;
import at.chipkarte.client.kse.soap.Konsultationsdaten;

import javax.swing.JSplitPane;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;

public class HauptFenster extends JFrame 
{

	private IKonsultationsApplikation applikation;
	
	private JPanel contentPane;
	private JComboBox cbReader;
	private JComboBox cbBehandlungsfall;
	private JSpinner spDatum;
	private JList<Konsultationsdaten> jlKonsultationen;
	private JComboBox cbFachgebiet;
	private JComboBox cbSvtTraeger;


	/**
	 * Create the frame.
	 */
	public HauptFenster(IKonsultationsApplikation applikation) 
	{
		this.applikation = applikation;
		try {
			initFrame();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}
	
	private void initFrame() throws Exception
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		cbReader = new JComboBox(applikation.alleReader());
		panel.add(cbReader, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		jlKonsultationen = new JList<Konsultationsdaten>();
		scrollPane.setViewportView(jlKonsultationen);
		
		JPanel panel_1 = new JPanel();
		splitPane.setRightComponent(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblDatum = new JLabel("Datum");
		panel_1.add(lblDatum);
		
		spDatum = new JSpinner();
		panel_1.add(spDatum);
		
		JLabel lblBehandlungsfall = new JLabel("Behandlungsfall");
		panel_1.add(lblBehandlungsfall);
		
		cbBehandlungsfall = new JComboBox(applikation.behandlungsFaelle().toArray());
		panel_1.add(cbBehandlungsfall);
		
		JLabel lblFachgebiet = new JLabel("Fachgebiet");
		panel_1.add(lblFachgebiet);
		
		cbFachgebiet = new JComboBox(applikation.fachGebiete().toArray());
		panel_1.add(cbFachgebiet);
		
		JLabel lblSvttrger = new JLabel("SVT-Tr\u00E4ger");
		panel_1.add(lblSvttrger);
		
		cbSvtTraeger = new JComboBox(applikation.kvTraeger().toArray());
		panel_1.add(cbSvtTraeger);
		
		JButton btnNeuErstellen = new JButton("Neu erstellen");
		btnNeuErstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_btnNeuErstellen_actionPerformed(e);
			}
		});
		panel_1.add(btnNeuErstellen);
		splitPane.setDividerLocation(150);
	}

	protected void do_btnNeuErstellen_actionPerformed(ActionEvent e) 
	{
		String behandlungsFall = (String)cbBehandlungsfall.getSelectedItem();
		CardReader reader = (CardReader)cbReader.getSelectedItem();
		String kvTraeger = (String)cbSvtTraeger.getSelectedItem();
		try {
			applikation.erfassen(reader, behandlungsFall, null, kvTraeger, new Date());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			JOptionPane.showMessageDialog(this,  e1.getMessage());
		}
	}
}
