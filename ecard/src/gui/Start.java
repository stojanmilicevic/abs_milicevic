package gui;

import java.awt.EventQueue;

import applikation.IKonsultationsApplikation;

public class Start 
{
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// TODO Implementierung verwenden
		IKonsultationsApplikation applikation = null; 
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HauptFenster frame = new HauptFenster(applikation);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
