package applikation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.chipkarte.client.base.soap.CardReader;
import at.chipkarte.client.kse.soap.Konsultationsdaten;

public class KonsultationsApplikationTest 
{
	private IKonsultationsApplikation applikation;
	
	private CardReader oCardReader;
	private CardReader eCardReader1;
	private CardReader eCardReader2;
	

	@Before
	public void setUp() throws Exception 
	{
		applikation = new DummyApplikation();  // TODO Implementierung
		CardReader[] readers = applikation.alleReader();
		oCardReader = readers[0];
		eCardReader1 = readers[1];
		eCardReader2 = readers[2];
	}

	@After
	public void tearDown() throws Exception 
	{
		applikation.abmelden();
	}

	@Test
	public void testAnmelden() throws Exception 
	{
		assertFalse(applikation.anmelden(oCardReader, "1111"));
		assertFalse(applikation.anmelden(eCardReader1, "0000"));
		assertTrue(applikation.anmelden(oCardReader, "0000"));
	}
	
	@Test
	public void testSuchen() throws Exception
	{
		assertTrue(applikation.anmelden(oCardReader, "0000"));
		Konsultationsdaten[] daten = applikation.suchen(eCardReader1);
		assertTrue(daten.length > 0);
		assertEquals("1003250229", daten[0].getVersichertenDaten().getSvNummer());
		
	}

}




